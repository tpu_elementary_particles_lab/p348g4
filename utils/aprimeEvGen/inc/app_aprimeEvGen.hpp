/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_APP_APRIME_EVGEN_MDL_H
# define H_APP_APRIME_EVGEN_MDL_H

# include "app/task_driven.hpp"
# include "app/mixins/root.hpp"

class TCanvas;

namespace aprime {

class Application : public p348::TaskDrivenApplication,
                    public p348::mixins::RootApplication {
public:
    typedef TaskDrivenApplication           Parent;
    typedef RootApplication                 RootParent;
    typedef typename Parent::Config         Config;
    typedef typename Parent::TaskCallback   TaskCallback;
    typedef typename Parent::TaskCleaner    TaskCleaner;
protected:
    virtual std::vector<p348::po::options_description> _V_get_options() const override;
    virtual void _V_configure_concrete_app() override;
    /// Define order of inherited running.
    //virtual int _V_run() final {
    //    return Parent::_V_run( vm, argc, argv ) |
    //           RootParent::_V_run( vm, argc, argv );
    //}
public:
    Application( Config * );
    virtual ~Application() {}
};

# define REGISTER_APRIME_EVGEN_TASK( name, descr, cllb, clnrCllb )                  \
    _BASE_GOO_TD_APP_PUSH_TASK( aprime::Application, name, descr, cllb, clnrCllb )

}  // namespace aprime

# endif  // H_APP_APRIME_EVGEN_MDL_H



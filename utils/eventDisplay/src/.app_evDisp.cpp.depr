/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "app_evDisp.hpp"
# include "ed_detector.hpp"

# include <TEveManager.h>
# include <TEveBrowser.h>
# include <TEveViewer.h>
# include <TEveScene.h>
# include <TGTab.h>
# include <TApplication.h>
# include <TEveArrow.h>

namespace p348 {
namespace evd {

void XXX_fill_placements();  // xxx

std::unordered_map<std::string, Application::abstract_detector_constructor> *
        Application::_detConstructors = nullptr;

Application::Application( po::variables_map * vmPtr ) :
        p348::AbstractApplication( vmPtr ),
        p348::mixins::RootApplication( vmPtr, "NA64 Event Display" ) {}

Application::~Application() {}

std::vector<po::options_description>
Application::_V_get_options() const {
    std::vector<po::options_description> res;
    {
        po::options_description connOpts("Event display connection");
        connOpts.add_options()
        ("ed.source",
            po::value<std::string>()->default_value( "somewhere" ),
            "TODO")
        // TODO: cfg parameters declarations ...
        //("g4.visMacroFile",
        //    po::value<std::string>()->default_value("vis.mac"),
        //    "'vis' run-time script")
        ;
        res.push_back( connOpts );
    }

    return res;
}

int
Application::_V_run() {
    XXX_fill_placements();

    if( !_detConstructors || _detConstructors->empty() ) {
        p348g4_loge( "No detectors available in this run. Aborting.\n" );
        return EXIT_FAILURE;
    }

    p348g4_log1("Constructing etectors (%d):\n", (int) _placements.size() );
    for( auto it  = _placements.begin();
              it != _placements.end(); ++it ) {
        p348g4_log2( "  - " ESC_CLRGREEN "%16s" ESC_CLRCLEAR
                     " at {%.1e, %.1e, %.1e}x{%.1e, %.1e, %.1e} ... ",
                     it->first.c_str(),
                     it->second.position.r[0], it->second.position.r[1], it->second.position.r[2],
                     it->second.rotation.byAxis[0],
                     it->second.rotation.byAxis[1],
                     it->second.rotation.byAxis[2] );
        auto ctrIt = _detConstructors->find( it->first );
        if( _detConstructors->end() == ctrIt ) {
            message( 2, "constructor not found --- " ESC_CLRYELLOW
                        "skipped" ESC_CLRCLEAR ".\n", true );
            continue;
        }
        ctrIt->second();
        message( 2, ESC_CLRGREEN "constructed" ESC_CLRCLEAR ".\n", true );
    }

    //_detectors.push_back( new ECAL() );  // xxx

    TEveManager::Create();
    p348g4_log2( "TEveManager instance created.\n" );

    // Create an arrow from O(0, 0, 0) to (0, 0, 15cm)
    TEveArrow * zArr = new TEveArrow( 0, 0, 500,
                                      0, 0, 0 );
    zArr->SetMainColor( kCyan );
    gEve->AddElement( zArr );

    // Draw detectors:
    char scNameBf[64];
    for( auto it  = _detectors.begin();
              it != _detectors.end(); ++it ) {
        snprintf( scNameBf, sizeof(scNameBf), "det-scene-%s", it->first.c_str() );
        TEveScene * scene = gEve->SpawnNewScene(scNameBf, "A detector's scene.");
        scene->SetHierarchical(kTRUE);
        it->second->draw_geometry( scene );
        gEve->GetDefaultViewer()->AddScene( scene );
    }
    if( _detectors.empty() ) {
        p348g4_loge( "No detectors described in this event monitor application instance!\n" );
    }

    gEve->GetBrowser()->GetTabRight()->SetTab(1);
    gEve->Redraw3D(kTRUE);

    get_TApplication().Run();
    return EXIT_SUCCESS;
}

void
Application::add_detector_instance( AbstractDetector * instancePointer ) {
    auto insertionResult =
        _detectors.insert( DECLTYPE(_detectors)::value_type(
            instancePointer->detector_name(),
            instancePointer ) );
    if( ! insertionResult.second ) {
        emraise( nonUniq, "Unable to insert detector instance `%s'.",
                 instancePointer->detector_name().c_str() );
    }
    // TODO: further code for hit dispatching
}

void
Application::_V_configure_concrete_app() {
    // Called after most of ROOT stuff is performed.
    // TODO: cfg parameters treatment...
}

// Detector dictionary
/////////////////////

void
Application::add_detector_constructor(
        const std::string & name,
        Application::abstract_detector_constructor ctr ) {
    typedef std::remove_pointer<DECLTYPE(_detConstructors)>::type DetDict;
    if( !_detConstructors ) {
        _detConstructors = new DetDict;
    }
    DetDict & detDict = *_detConstructors;
    auto insertionResult = detDict.insert( DetDict::value_type(name, ctr) );
    if( !insertionResult.second ) {
        emraise( nonUniq,
                 "Couldn't insert detector constructor \"%s\".",
                 name.c_str() );
    }
}

Application::abstract_detector_constructor
Application::get_detector_constructor( const std::string & name ) {
    typedef std::remove_pointer<DECLTYPE(_detConstructors)>::type DetDict;
    assert( goo::app<Application>()._detConstructors );
    DetDict & detDict = *goo::app<Application>()._detConstructors;
    auto it = detDict.find( name );
    if( detDict.end() == it ) {
        list_detector_constructors( std::cerr );
        emraise( noSuchKey,
                 "Couldn't find detector constructor \"%s\".",
                 name.c_str() );
    }
    return it->second;
}

void
Application::list_detector_constructors( std::ostream & o ) {
    o << "Available detector constructors:" << std::endl;
    if( !_detConstructors || _detConstructors->empty() ) {
        o << "  <no detectors available>" << std::endl;
        return;
    }
    for( auto it  = _detConstructors->begin();
              it != _detConstructors->end(); ++ it ) {
        o << "  " << it->first << std::endl;
    }
}

std::string
Application::form_detector_name( const std::string & templateName ) {
    std::string result = templateName;
    if( std::string::npos != templateName.find("$") ) {
        // TODO: substitute number XXX?
    }
    //if( std::string::npos != templateName.find("$") )
    return result;
}

// Placement
///////////

void
Application::add_detector_placement(
            const std::string & detName,
            const DetectorPlacement & placement ) {
    /*auto insertionResult =*/ goo::app<Application>()._placements.insert(
        DECLTYPE(_placements)::value_type(detName, placement) );
    //if( ! insertionResult.second ) {
    //    emraise( nonUniq,
    //             "Couldn't insert placement for detector \"%s\".",
    //             detName.c_str() );
    //}
}

const DetectorPlacement &
Application::detector_placement( const std::string & detName ) {
    auto it = goo::app<Application>()._placements.find( detName );
    if( goo::app<Application>()._placements.end() == it ) {
        emraise( noSuchKey,
                 "Couldn't find placement data for detector \"%s\".",
                 detName.c_str() );
    }
    return it->second;
}

}  // namespace evd
}  // namespace p348


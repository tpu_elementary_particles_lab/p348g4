# ifndef H_EVENT_DISPLAY_APPLICATION_H
# define H_EVENT_DISPLAY_APPLICATION_H

/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# include "app/mixins/root.hpp"
# include "app/mixins/alignment.hpp"
# include "ed_PaletteUpdater.hpp"
# include <boost/date_time/posix_time/posix_time.hpp>

# include "ed_EventReceiver.hpp"
# include "p348g4_uevent.hpp"

class ControlGUI;
class TGLAnnotation;
class TEveRGBAPalette;
class TGCommandPlugin;

class P348CommandWidgetPlugin;

namespace p348 {

namespace alignment {
class DetectorConstructorsDict;
class DetectorPlacement;
class DetectorsSet;
struct AbstractTrackReceptiveVolume;
}

namespace evd {

class EventReceiver;
class PaletteUpdater;

/**@class Application
 * @brief NA64 event display application class.
 * @ingroup evd
 *
 * The main goal of event display application is to provide representative
 * real-time overview of real physical events currently occuring on
 * experimental facility. Since it does not imply any comprehensive analysis,
 * though it should be fast and robust enough to provide as more rough
 * estimations of detector apparatus states as possible without any tangible
 * performance lack.
 *
 * The application can visualize a single event (that can be useful in offline
 * analysis) as well as persistent mode when several events are visualized
 * atonce with FIFO updating. FIFO event buffer is just a simple circular buffer
 * of variable length.
 *
 * Displaying of averaged measurements also available with special checkbox on
 * GUI. When averaging is enabled, it will be done according to persistency
 * setting (number of currently events stored after acquizition and
 * deserialization).
 *
 * TODO: playback handlers are still rudimentary.
 *
 * A boost::statechart is behind of this app FSM implementation.
 */
class Application : public mixins::RootApplication,
                    public p348::evd::EventReceiver::StateManifestingAdapter,
                    public mixins::AlignmentApplication {
private:
    EventReceiver * _cReceiver;  ///< Network data receiver instance ptr.
    ControlGUI * _cGUI;  ///< Controlling GUI extras.

    P348CommandWidgetPlugin * _cmdPlPtr;  ///< Custom cmd-line widget.

    boost::mutex _uiMtx;  ///< GUI redraw synchronization mutex.
    boost::condition_variable _uiCondition;  ///< «GUI redraw done» notifier.
    bool _uiFree;   ///< «GUI redraw done» notifier control.

    /// Data receiving reentrant circular buffer.
    ::p348::evd::aux::LeasingPool::Block _reentrantSerializedMessageBuffer;
    /// Data receiving reentrant message instance.
    ::p348::events::MulticastMessage _reentrantMessageInstance;

    PaletteUpdater * _sadcPaletteUpdater,
                   * _apvPaletteUpdater
                   ;
    
    /// Stores palette-to-detector association for track receptive volumes.
    std::unordered_map<const alignment::AbstractTrackReceptiveVolume*, PaletteUpdater *> _det2palette;

    /// Procedure creating scenes for different entites (common
    /// detectors, hits, tracks, etc.)
    void _create_scenes();
protected:
    virtual std::vector<po::options_description> _V_get_options() const override;
    virtual void _V_configure_concrete_app() override;
    virtual int _V_run() override;
private:
    boost::posix_time::ptime _lastRedrawTime;
    bool _isConnected, _isPlaying;
    TGLAnnotation * _annotationPtr;  // todo: make configurable

    /// The instance of TGCommandPlugin representing command line.
    TGCommandPlugin * _cmdPluginPtr;
protected:
    virtual void _V_on_idle() override;
    virtual void _V_on_listening() override;
    virtual void _V_on_pause() override;
    virtual void _V_on_play() override;
    virtual void _V_resolve_connection_payload( p348::evd::aux::MessageReceiver * const & ) override;
    virtual void _V_on_malfunction( const goo::Exception & details ) override;
    /// Returns true, if event in current reentrant buffer was drawn.
    bool _emplace_representative_event();
    void _update_event_navigation_buttons();
    /// Parses options string and returns new updater instance (with new
    /// associated palette instance).
    virtual PaletteUpdater * _new_palette_updater_by_optstring( const std::string & ) const;
public:
    /// Event display application ctr.
    Application( po::variables_map * vmPtr );
    /// Event display application dtr.
    virtual ~Application();

    /// Returns current receiver object. Can be invoked only
    /// when receiver object is created. Returns mutable ref.
    EventReceiver & receiver() { assert(_cReceiver); return *_cReceiver; }
    /// Returns current receiver object. Can be invoked only
    /// when receiver object is created. Returns constant ref.
    const EventReceiver & receiver() const { assert(_cReceiver); return *_cReceiver; }

    /// Returns true, when there are event in queue.
    bool has_queued_events() const;
    /// Causes application to fetch new events from the queue.
    void emplace_new_events();

    /// Returns GUI locking mutex. Useful for synchronization
    /// access to ROOT::GUI content of TEve widgets.
    boost::mutex & ui_mutex() { return _uiMtx; }
    /// Notifying variable for GUI redraw.
    boost::condition_variable & ui_condition() { return _uiCondition; }
    /// Returns true when GUI is ready for change.
    bool ui_free() const { return _uiFree; }

    /// Returns true, when something changed on 3D scene where detector,
    /// hits and tracks could be redrawn according to current settings.
    bool last_redraw_foul() const;
    /// Sets last redraw time to current. Next invokation of last_redraw_foul()
    /// will count from this method invokation time.
    void update_last_redraw_time();

    /// Returns true, when wvents playback enabled.
    bool is_playing() const;
    /// Causes underlying FSM transit to «playing» state.
    void play();
    /// Causes underlying FSM transit to «paused» state.
    void pause();

    /// Returns true, if undelying FSM is in connected state with
    /// valid associated connection.
    bool is_connected() const;
    /// Queues «connect» transition to underlying FSM.
    void connect();
    /// Queues «disconnect» transition to underlying FSM.
    void disconnect();

    /// Returns true if averaged values drawing is enabled.
    bool do_draw_averaged() const;
    /// Returns current persistency setting: number of events to be
    /// displayed at the moment.
    size_t persistency() const;
    /// Dispatches signal about changed persistency among receivers.
    void persistency( size_t );

    /// Substitutes current annotation text with provided one.
    void annotate( const std::string & );
    /// Substitutes current annotation text formed according to current experimental
    /// event.
    void annotate( const events::ExperimentalEvent & );
    /// Returns true when annotation is allewed by application configuration.
    bool has_annotation() const { return _annotationPtr; }

    # if 0
    /// Common SADC RGBA palette getter (mutable).
    TEveRGBAPalette & palette_common_SADC();
    /// Common SADC RGBA palette getter (const).
    const TEveRGBAPalette & palette_common_SADC() const;
    /// Updates upper limit of common SADC RGBA palette.
    void update_palette_common_SADC( double upVal );
    /// Makes palette preserve current state ignoring updating invokations. The
    /// control via update_...() call is usually suitable for detectors classes
    /// while to ignore their call and subdue to GUI one need to temporarily
    /// disable this updating.
    void lock_palette_common_SADC() { _commonSADCPalette_isLocked = true; }
    /// Makes palette react updating invokations.
    void unlock_palette_common_SADC() { _commonSADCPalette_isLocked = false; }

    /// Common APV RGBA palette getter (mutable).
    TEveRGBAPalette & palette_common_APV();
    /// Common APV RGBA palette getter (const).
    const TEveRGBAPalette & palette_common_APV() const;
    /// Updates upper limit of common APV RGBA palette.
    void update_palette_common_APV( double upVal );
    /// Makes palette preserve current state ignoring updating invokations. The
    /// control via update_...() call is usually suitable for detectors classes
    /// while to ignore their call and subdue to GUI one need to temporarily
    /// disable this updating.
    void lock_palette_common_APV() { _commonAPVPalette_isLocked = true; }
    /// Makes palette react updating invokations.
    void unlock_palette_common_APV() { _commonAPVPalette_isLocked = false; }
    // TODO: ...plaette UI control?
    # endif
    PaletteUpdater & common_SADC_palette_updater() { return *_sadcPaletteUpdater; }
    PaletteUpdater &  common_APV_palette_updater() { return  *_apvPaletteUpdater; }
    const PaletteUpdater & common_SADC_palette_updater() const { return *_sadcPaletteUpdater; }
    const PaletteUpdater &  common_APV_palette_updater() const { return  *_apvPaletteUpdater; }

    /// Setups an associattion between track receptive volume and
    /// its track points palette.
    void associate_palette_with_tracking_detector( const alignment::AbstractTrackReceptiveVolume *,
                                                   PaletteUpdater * );
    /// Returns palette updater for track points instance asssociated with particular
    /// track receptive volume.
    PaletteUpdater * palette_for( const alignment::AbstractTrackReceptiveVolume * );
};  // class Application

}  // namespace evd
}  // namespace p348

# endif  /* H_EVENT_DISPLAY_APPLICATION_H */



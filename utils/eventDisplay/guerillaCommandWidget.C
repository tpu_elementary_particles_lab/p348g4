/**@file guerillaCommandWidget.C
 * @brief Guerilla patch for TEve's browser accessing
 * */

struct MorozoffCommandPl : public TGCommandPlugin {
    TGTextView * text_view_ptr() { return fStatus; }
};

void guerillaCommandWidget() {
    auto b = TEveManager::Create();
    TGFrameElement * fe = static_cast<TGFrameElement *>(b->GetBrowser()->GetTabBottom()->GetTabContainer(0)->GetList()->First());
    MorozoffCommandPl * guerillaPtr = reinterpret_cast<MorozoffCommandPl *>( static_cast<TGCommandPlugin *>(fe->fFrame) );

    const TGFont *font = gClient->GetFont("-*-fixed-*-*-*-*-14-*-*-*-*-*-*-*");
    if( !font ) {
        font = gClient->GetResourcePool()->GetDefaultFont();
    }
    FontStruct_t labelfont = font->GetFontStruct();
    guerillaPtr->text_view_ptr()->SetFont( labelfont );
    guerillaPtr->text_view_ptr()->AddLine( "More blood soaked the soil." );
}


    //gEve->GetBrowser()->HideBottomTab();  // hides command line
    //gEve->GetBrowser()->GetTabBottom();  // bottom tab containing command line

    //TEveManager::Create();
    //TGFrameElement * fe = static_cast<TGFrameElement *>(b->GetBrowser()->GetTabBottom()->GetTabContainer(0)->GetList()->First());
    //static_cast<TGCommandPlugin *>(fe->fFrame)->

    // Changes the font for TGTextView descendants:
    # if 0
    const TGFont *font = gClient->GetFont("-*-fixed-*-*-*-*-14-*-*-*-*-*-*-*");
    if( !font ) {
        font = gClient->GetResourcePool()->GetDefaultFont();
    }
    FontStruct_t labelfont = font->GetFontStruct();
    fTextView->SetFont( labelfont );
    fTextView->AddLine( "More blood soaked the soil." );
    # endif
    # if 0
    TEveWindowSlot      * slot = NULL;
    TEveWindowFrame     * frame = NULL;

    TEveViewer * v = 0;

    slot = TEveWindow::CreateWindowInTab(gEve->GetBrowser()->GetTabRight());
    TEveWindowPack* pack1 = slot->MakePack();
    pack1->SetShowTitleBar(kFALSE);
    pack1->SetHorizontal();

    // Embedded viewer.
    slot = pack1->NewSlot();
    v = new TEveViewer("BarViewer");
    v->SpawnGLEmbeddedViewer(gEve->GetEditor());
    slot->ReplaceWindow(v);
    v->SetElementName("Bar Embedded Viewer");

    gEve->GetViewers()->AddElement(v);
    v->AddScene(gEve->GetEventScene());

    slot = pack1->NewSlot();
    TEveWindowPack* pack2 = slot->MakePack();
    pack2->SetShowTitleBar(kFALSE);

    slot = pack2->NewSlot();
    slot->StartEmbedding();
    TCanvas* can = new TCanvas("Root Canvas");
    can->ToggleEditor();
    slot->StopEmbedding();

    // SA viewer.
    slot = pack2->NewSlot();
    v = new TEveViewer("FooViewer");
    v->SpawnGLViewer(gEve->GetEditor());
    slot->ReplaceWindow(v);
    gEve->GetViewers()->AddElement(v);
    v->AddScene(gEve->GetEventScene());
    # endif

# Config file, specifying some parameters manually for points preprocessing.

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

manualParameters = {
    'SN'    : { 'cFactorX' : .7 },
    'LSND'  : { 'cFactorX' : .7 },
    'E137'  : { 'cFactorX' : .7 },
    'CHARM' : { 'cFactorX' : .7 },
    'U70'   : { 'cFactorX' : .7 },
    'Orsay' : { 'cFactorX' : .7 },
    'E141'  : { 'cFactorX' : .7 },
    'E774'  : { 'cFactorX' : .7 },
    'E141_' : { 'cFactorX' : .7 },
    'E774_' : { 'cFactorX' : .7 },
    'KLOE'  : {
        'centerX' : .1,
        'centerY' : .1
    },
    'KLOE_'  : {
        'centerX' : .1,
        'centerY' : .1
    },
    'APEX-MAMI test' : {
        'centerX' : .25,
        'centerY' : .2
    },
    'DarkLight' : {
        'centerX' : .002,
        'centerY' : .001,
    },
    'MESA' : {
        'centerX' : .02,
        'centerY' : .005
    },
    'MAMI' : {
        'centerX' : .1,
        'centerY' : .02
    },
    'BaBar' : {
        'centerX' : .8,
        'centerY' : .04
    },
    'WASA' : {
        'centerX' : .03,
        'centerY' : .015
    },
    'HPS' : {
        'centerX' : 1.045e-1,
        'centerY' : 3e-5
    },
    'APEX' : {
        'centerX' : 0.15,
        'centerY' : .02
    },
    'VEPP-3' : {
        'centerX' : .01,
        'centerY' : .02
    },
    'HPS2' : {
        'centerX' : 0.1,
        'centerY' : 1e-2
    }
}


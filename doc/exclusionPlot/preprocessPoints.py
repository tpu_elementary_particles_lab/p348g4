#!/bin/env python3

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
The script provides some pre-processing operations for manually added points
(see http://arohatgi.info/WebPlotDigitizer/app/ for JS-tool): finds an
average point for each dataset, reorder data set to obtain a hull,
intepolate it in polar coordinates and write out.

By default, it will plot resulting data using gnuplot util.

For available options, see --help option.
"""

import json
from subprocess import Popen, PIPE, STDOUT
from math import sqrt, atan2, log, exp, pi, cos, sin
from operator import itemgetter
from scipy import interpolate as interMod
from numpy import arange
from sys import argv
import argparse
#from string import join as join_strings

# Template strings
commonPlotHdr = """# This is an automatically generated script.
# Manual changes aren't recommended.
"""
stagePlottingCommands = {
    'source'                : {
        'commonHeader' : "set log xy\nplot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s.dat' u 1:2 w linespoints title \"%(SetName)s points\"",
    },
    'polar'                 : {
        'commonHeader' : "set polar\nplot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s.dat' u 4:3 w linespoints title \"%(SetName)s points\"",
    },
    'interpolated-linear'   : {
        'commonHeader' : "plot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s_interp.dat' u 2:1 w lines title \"%(SetName)s interp\", '%(wd)s/%(SetName)s.dat' u 4:3 w linespoints title \"%(SetName)s points\"",
    },
    'interpolated-polar'    :  {
        'commonHeader' : "set polar\nplot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s_interp.dat' u 2:1 w lines title \"%(SetName)s interp\", '%(wd)s/%(SetName)s.dat' u 4:3 w linespoints title \"%(SetName)s points\"",
    },
    'final'                 :  {
        'commonHeader' : "set log xy\nset style fill  transparent solid 0.50 noborder\nplot ",
        'dsPlotLine'   : "'%(wd)s/%(SetName)s_interp.dat' u 3:4 w filledcurves closed title \"%(SetName)s\""
    }
}

def sort_set_clockwise( srcSet, centerX=None,
                                centerY=None,
                                cFactorX=1.,
                                cFactorY=1.,
                                smoothFactor=None,
                                interpolationMethod=None):
    pSet = srcSet[:]  # copy
    # find center point:
    if not centerX:
        if centerY:
            raise Exception('X coordinate for center is not specified while Y was.')
        centerX = log(pSet[0][0])
        centerY = log(pSet[0][1])
        n = 0
        for p in pSet[1:]:
            centerX += log(p[0])
            centerY += log(p[1])
            n += 1
        centerX /= cFactorX*n  # just shift...
        centerY /= cFactorY*n
    else:
        centerX = log(centerX)
        centerY = log(centerY)
    #print( "center: %e %e"%(exp(centerX), exp(centerY)) )
    for p in pSet:
        x = log(p[0])
        y = log(p[1])
        dx = x - centerX
        dy = y - centerY
        pRho = sqrt( dx**2 + dy**2 )
        pTheta = atan2(dy, dx)
        #if pTheta < 0:
        #    pTheta = 2*pi + pTheta
        #print( "rho = %e, theta = %e"%(pRho, pTheta) )
        p.append(pRho)
        p.append(pTheta)
    return [sorted( pSet, key=itemgetter(3) ), [centerX, centerY]]

if "__main__" == __name__:
    #
    # Argument parser.
    parser = argparse.ArgumentParser(description="Exclusion plot for A' script.")
    parser.add_argument('datasets', metavar='dataset', action='append', nargs='*',
                    help='Data set to be processed. If unset, then all found datasets will be processed.')
    parser.add_argument('--stage',
                    choices=['source', 'polar', 'interpolated-linear', 'interpolated-polar', 'final'],
                    default='final',
                    help='Specifies which on which stage data is to be plotted.')
    parser.add_argument('--listDatasets', action='store_true',
                    help='Just prints out the data set names and quits.')
    parser.add_argument('--wd',
                    default='/tmp',
                    help='Working directory.')
    parser.add_argument('-i', '--jsonSource',  # TODO: make it list!
                    #default="../../p348g4/doc/exclusionPlot/nData/wpd_plot_data.json",
                    type=argparse.FileType('r'),
                    help='Just prints out the data set names and quits.',
                    action='append')
    parser.add_argument('--dumpGPScript', action='store_true',
                    help='Prints gnuplot script to stdout.')
    parser.add_argument('--noplot', action='store_true',
                    help='Do not launch a gnuplot subprocess after processing.')
    parser.add_argument('-c', '--manualSpecs',
                    type=argparse.FileType('r'),
                    help='Manual parameters for named data sets specification file.')
    args = parser.parse_args()

    #
    # Do the things.

    # Read manual specificated instructions, if provided.
    manualParameters = {}
    if args.manualSpecs:
        exec( args.manualSpecs.read() )

    # Read out JSON numericals (http://arohatgi.info/WebPlotDigitizer/app/ format)
    jdataStr = ""
    for jsonSource in args.jsonSource:
        with jsonSource as jdatFile:
            jdataStr=jdatFile.read()
        jDat = json.loads(jdataStr)

        if args.listDatasets:
            #print( 'Available datasets in %s'%args.jsonSource )
            for dataSet in jDat['wpd']['dataSeries']:
                print(dataSet['name'])
            exit(0)

        # Process each choosen (or all found) sets
        plotStrTuple = []
        for dataSet in jDat['wpd']['dataSeries']:
            if len(args.datasets[0]):
                if not dataSet['name'] in args.datasets[0]:
                    continue
                else:
                    print( dataSet['name'] )
            # prepare formatting dictionary
            fmtDct = {
                'wd' : args.wd,
                'SetName' : dataSet['name']
            }
            # extract points from JSON
            sortedPoints = []
            hullParameters = None
            extracted2sort = list(map( lambda s: [s['value'][0], s['value'][1]], dataSet['data'] ))
            # sort in polar coordinates
            if dataSet['name'] in manualParameters.keys():
                sortedPoints, hullParameters = sort_set_clockwise( extracted2sort, **(manualParameters[dataSet['name']]) )
            else:
                sortedPoints, hullParameters = sort_set_clockwise( extracted2sort )
            # write sorted data [x, y, rho, theta]
            outFile = open( '%(wd)s/%(SetName)s.dat'%fmtDct, 'w' )
            for p in sortedPoints:
                outFile.write( "%e\t%e\t%e\t%e\n"%(p[0], p[1], p[2], p[3]))
            outFile.close()
            if args.stage != 'source' and args.stage != 'polar':
                # interpolate
                interpolatedF = None
                if dataSet['name'] in manualParameters and \
                   'interpolationMethod' in manualParameters[dataSet['name']]:
                    if manualParameters[dataSet['name']]['interpolationMethod'] == 'UnivariateSpline':
                        smoothFactor = 0.9
                        if dataSet['name'] in manualParameters and \
                            'smoothFactor' in manualParameters[dataSet['name']]:
                            smoothFactor = manualParameters[dataSet['name']]['smoothFactor']
                        interpolatedF = interMod.UnivariateSpline(
                            list(map(lambda p: p[3], sortedPoints)),
                            list(map(lambda p: p[2], sortedPoints)),
                            s=smoothFactor)
                else:
                    interpolatedF = interMod.interp1d(
                        list(map(lambda p: p[3], sortedPoints)),
                        list(map(lambda p: p[2], sortedPoints)),
                        kind='cubic')
                try:
                    interpolated = list(map(
                        lambda arg: [ arg, float(interpolatedF(arg)) ],
                        list([ x for x in arange( sortedPoints[0][3],
                                                  sortedPoints[-1][3],
                                                  (sortedPoints[-1][3] - sortedPoints[0][3])/210 )
                            ])
                        ))
                except ValueError as e:
                    print( "In data set \"%s\""%dataSet['name'] )
                    raise e
                # write interpolated data
                outFInt = open( '%(wd)s/%(SetName)s_interp.dat'%fmtDct, 'w' )
                for p in interpolated:
                    rho = p[1]
                    theta = p[0]
                    x = exp(rho*cos(theta) + hullParameters[0])
                    y = exp(rho*sin(theta) + hullParameters[1])
                    outFInt.write( "%e\t%e\t%e\t%e\n"%( rho, theta, x, y ) )
                outFInt.close()
            # append plotting string with one
            plotStrTuple.append( stagePlottingCommands[args.stage]['dsPlotLine']%fmtDct )
        # now plot all the stuff
        plotStr = commonPlotHdr + '\n' + stagePlottingCommands[ args.stage ]['commonHeader'] + \
                                  ', \\\n'.join( plotStrTuple ) + '\n'
        if args.dumpGPScript:
            print( plotStr )
        if not args.noplot:
            p = Popen(['gnuplot', '-persistant'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
            gpOut = p.communicate(input=plotStr.encode('utf-8'))[0]
            if gpOut:
                print(gpOut.decode('utf-8'))


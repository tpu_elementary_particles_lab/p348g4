List of vessels:

1) vac_vessel.gdml -- this vacuum vessel is utilized for minimizing secondary particles
interactions.

2) mag_vessel.gdml -- this vacuum vessel is located inside magnet. So, magnet
field should be defined inside this vessel.

<?xml version="1.0" encoding="UTF-8" ?>
{# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 # Author: Renat R. Dusaev <crank@qcrypt.org>
 # 
 # Permission is hereby granted, free of charge, to any person obtaining a copy of
 # this software and associated documentation files (the "Software"), to deal in
 # the Software without restriction, including without limitation the rights to
 # use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 # the Software, and to permit persons to whom the Software is furnished to do so,
 # subject to the following conditions:
 # 
 # The above copyright notice and this permission notice shall be included in all
 # copies or substantial portions of the Software.
 # 
 # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 # FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 # COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 # IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 # CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 #}
<!DOCTYPE gdml>
<gdml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd">
	<define>
		{#parameters#}
		<variable name="magnetWidth_mm" 				value="50"/>		
		<variable name="magnetLength_mm" 				value="500"/>
		<variable name="distanceMagnetToCenter_mm" 		value="100"/>
		
		{#technical definitions#}
		<variable name="worldRadiusMax_mm"				value="distanceMagnetToCenter_mm + magnetWidth_mm * 3 / 4"/>
		<variable name="worldRadiusMin_mm" 				value="distanceMagnetToCenter_mm - magnetWidth_mm / 2"/>
	
		<quantity name="magnetWidth"	 				value="magnetWidth_mm" 			unit="mm"/>
		<quantity name="magnetLength" 					value="magnetLength_mm" 		unit="mm"/>
		
	</define>
	
	<solids>
		
		<tube name="geoMagnets" 		rmin="worldRadiusMin_mm" 			rmax="worldRadiusMax_mm"
										z="magnetLength_mm"
										deltaphi="360" 						startphi="0"
										aunit="deg" 						lunit="mm"/>
										
		<tube name="geoMagnetMother" 	rmin="worldRadiusMin_mm" 			rmax="worldRadiusMax_mm"
										z="magnetLength_mm"
										deltaphi="90" 						startphi="-45"
										aunit="deg" 						lunit="mm"/>
										
		<box name="geoMagnet" 			x="magnetWidth"
										y="magnetWidth"
										z="magnetLength"/>
									
	</solids>

	<structure>
		
		<volume name="magnet">
			<solidref ref="geoMagnet"/>
			<materialref ref="G4_AIR"/>
			<auxiliary auxtype="style" auxvalue="*:!surface"/>
		</volume>
		
		<volume name="magnetMother">
			<solidref ref="geoMagnetMother"/>
			<physvol>
				<volumeref ref="magnet"/>
				<position x="distanceMagnetToCenter_mm" 	unit="mm"/>
			</physvol>
			<auxiliary auxtype="style" auxvalue="*:!wireframe"/>
		</volume>
		
		<volume name="magnets">
			<solidref ref="geoMagnets"/>
			<replicavol number="4">
				<volumeref ref="magnetMother"/>
                <replicate_along_axis>
					<direction phi="1"/>
					<width value="90" 		unit="deg"/>
					<offset value="45" 		unit="deg"/>
                </replicate_along_axis>
			</replicavol>
			<auxiliary auxtype="style" auxvalue="*:!hidden"/>
		</volume>
		
	</structure>
	
	<setup name="Default" version="1.0">
		<world ref="magnets"/>
	</setup>
</gdml>

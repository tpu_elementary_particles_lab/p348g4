#!/bin/env python
# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

"""
A GDML parser implementation making interconnection with ROOT.
"""

import xml.etree.ElementTree as ET
from ROOT import gROOT

def parse_gdml( rootFilePath, acts ):
    tree = ET.parse( rootFilePath )
    root = tree.getroot()
    #for child in root:
    #    print( child.tag, child.attrib )
    for constant in root.findall('./define/constant'):
        acts.gdml_variable( constant.attrib )
    for variable in root.findall('./define/variable'):
        acts.gdml_variable( variable.attrib )
    for quantity in root.findall('./define/quantity'):
        acts.gdml_quantity( quantity.attrib )

if "__main__" == __name__:
    #import argparse as AP
    #import sys, os
    ##sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    #from testActionsDict import GDMLActionsDictionary

    #ap = AP.ArgumentParser( description=__doc__, prog=sys.argv[0] )
    #ap.add_argument( "indexInputFile" )
    #ap.add_argument( "--working-dir", "-w",
    #                 default=None,
    #                 help="A root directory for GDML files structure." )
    #args = ap.parse_args()
    #actDict = GDMLActionsDictionary(False)
    #parse_gdml( args.indexInputFile, actDict )
    #print( actDict.definitions['variables'] )
    ##print( os.path.abspath( args.indexInputFile ) )

    from testActionsDict import Scriptlet

    ctx = {'a' : 1, 'b' : 2 }
    l = Scriptlet()
    # This will just evaluate into context temporary variable '_'
    # and returning its value:
    res = l.evaluate( ctx, 'a+b' )
    print( "(dev #1) Evaluated to: ", res )
    # This have to append context with given variable name:
    res = l.evaluate( ctx, 'a-b', _='c' )
    print( "(dev #2) Evaluated to: ", res, ", c=", ctx['c'] )


#!/bin/bash

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
THIS_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

#ROOT_DIR=$DIR_AFS_SW/lcg/app/releases/ROOT/5.34.20/x86_64-slc6-gcc47-opt/root

source ../helpers/env

$CMAKE ../../p348g4 \
    -DCMAKE_C_COMPILER=$GCC_PREFIX/bin/gcc  \
    -DCMAKE_CXX_COMPILER=$GCC_PREFIX/bin/g++  \
    -DROOT_CONFIG_EXECUTABLE=$ROOT_DIR/bin/root-config \
    -DCMAKE_MODULE_PATH=/afs/cern.ch/user/r/rdusaev/Projects/goo.install/share/cmake/Modules/ \
    -DCMAKE_CXX_FLAGS="$CXX_CFLAGS" \
    -DBOOST_ROOT=$DIR_BOOST \
    -DBoost_NO_SYSTEM_PATHS=ON \
    -DBoost_USE_MULTITHREADED=ON \
    -DBoost_COMPILER="-gcc47" \
    -DBoost_NO_BOOST_CMAKE=ON \
    -DBoost_REALPATH=ON \
    -DBoost_ADDITIONAL_VERSIONS=1.55 \
    -DROOT_DIR=$ROOT_DIR/cmake/ \
    -DPROTOBUF_INCLUDE_DIR=$Protobuf_DIR/../../../include \
    -DPROTOBUF_LIBRARY=$Protobuf_DIR/../../../lib64/libprotobuf.so \
    -DPROTOBUF_PROTOC_EXECUTABLE=$Protobuf_DIR/../../../bin/protoc \
    -DEXP_RAW_DATA_READING=ON \
    -DANALYSIS_ROUTINES=ON \
    -DALIGNMENT_ROUTINES=ON \
    -DRPC_PROTOCOLS=ON \
    -DCMAKE_PREFIX_PATH=/online/soft/p348-daq/coral/src/DaqDataDecoding/src \
    -Dbuild_eventDisplay=ON \
    -Dbuild_evreading=ON \
    -DGSL_CONFIG_EXECUTABLE=$DIR_GSL/bin/gsl-config \
    -DROOT_CINT_EXECUTABLE=/afs/cern.ch/sw/lcg/app/releases/ROOT/6.04.06/x86_64-slc6-gcc49-opt/root/bin/rootcint \
    -DROOTCINT_EXECUTABLE=/afs/cern.ch/sw/lcg/app/releases/ROOT/6.04.06/x86_64-slc6-gcc49-opt/root/bin/rootcint \
    -DGENFIT_LIB_PATH=~/Projects/GenFit/lib/ \
    -DDDD_LIB_PATH=~/Projects/compass/coral_src/src/DaqDataDecoding/src/ \
    -DDATE_LIBMONITOR=/afs/cern.ch/user/r/rdusaev/public/DATE/monitoring/Linux/libmonitor.a
#    -LH
#    -DDATE_LIBMONITOR=/online/soft/date/monitoring/Linux/libmonitor.a

#-DCMAKE_MODULE_PATH=/afs/cern.ch/user/r/rdusaev/public/goo.install/share/cmake/Modules/ \
#-DCMAKE_PREFIX_PATH=$DIR_HOME/public/p348-daq/coral/src/DaqDataDecoding/src \

#echo $_CMAKE_PREFIX_PATH
#/afs/cern.ch/sw/geant4/releases/geant4.10.1.ref05/x86_64-slc6-gcc49-opt/lib64/Geant4-10.2.0/
#/afs/cern.ch/sw/lcg/external/Boost/1.55.0_python2.7/x86_64-slc6-gcc47-opt/
#/afs/cern.ch/sw/lcg/external/CMake/3.5.2/Linux-x86_64/bin/cmake
#/afs/cern.ch/user/r/rdusaev/3rdParty/protobuf.install/bin/protoc
#LD_LIBRARY_PATH=/afs/cern.ch/sw/lcg/contrib/gcc/4.9.3/x86_64-slc6/lib64/
#/afs/cern.ch/user/r/rdusaev/Projects/goo.install/
#/afs/cern.ch/user/r/rdusaev/3rdParty/dateV371/monitoring/Linux/libmonitor.a
#/afs/cern.ch/sw/lcg/app/releases/ROOT/6.02.12/x86_64-slc6-gcc49-opt/root/
#/afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.20/x86_64-slc6-gcc47-opt/root/
#LD_LIBRARY_PATH=/afs/cern.ch/sw/lcg/contrib/gcc/4.9.3/x86_64-slc6/lib64/:/afs/cern.ch/user/r/rdusaev/3rdParty/protobuf.install/lib64
#export ROOTSYS=/afs/cern.ch/sw/lcg/app/releases/ROOT/6.02.12/x86_64-slc6-gcc49-opt/root/
#-DBoost_NO_BOOST_CMAKE=ON \
#-DBOOST_INCLUDEDIR=$DIR_BOOST/include/ \
#-DBOOST_LIBRARYDIR=$DIR_BOOST/lib \
#-DCMAKE_CXX_FLAGS="-Wl,-rpath,$GCC_PREFIX/lib64 -isystem$DIR_BOOST/include/boost-1_55/ -L$DIR_BOOST/lib" \


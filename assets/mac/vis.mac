# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# TODO: set it from application
/control/macroPath ../../p348g4/assets/mac/

# Use this open statement to create an OpenGL view:
#/vis/open OGLSX 600x600-0+0
#/vis/open OGLSQt #600x600-0+0
/vis/open OGLI
#
# Use this open statement to create a .prim file suitable for
# viewing in DAWN:
#/vis/open DAWNFILE
#
# Use this open statement to create a .heprep file suitable for
# viewing in HepRApp:
#/vis/open HepRepFile
#
# Use this open statement to create a .wrl file suitable for
# viewing in a VRML viewer:
#/vis/open VRML2FILE
#
# Disable auto refresh and quieten vis messages whilst scene and
# trajectories are established:
#/vis/viewer/set/autoRefresh false
/vis/verbose errors
#
# Draw geometry:
/vis/drawVolume
#
#
# Specify zoom value:
/vis/viewer/zoom .4
#
# Specify style (surface or wireframe):
#/vis/viewer/set/style wireframe
#
# Draw coordinate axes:
#/vis/scene/add/axes 0 0 0 1 m
#
# Draw smooth trajectories at end of event, showing trajectory points
# as markers 2 pixels wide:

/vis/scene/add/trajectories smooth
/vis/modeling/trajectories/create/drawByCharge
/vis/modeling/trajectories/drawByCharge-0/default/setDrawStepPts true
/vis/modeling/trajectories/drawByCharge-0/default/setStepPtsSize 2

# (if too many tracks cause core dump => /tracking/storeTrajectory 0)
#
# Draw hits at end of event:
#/vis/scene/add/hits
#
# To draw only gammas:
#/vis/filtering/trajectories/create/particleFilter
#/vis/filtering/trajectories/particleFilter-0/add gamma
#
# To invert the above, drawing all particles except gammas,
# keep the above two lines but also add:
#/vis/filtering/trajectories/particleFilter-0/invert true
#
# Many other options are available with /vis/modeling and /vis/filtering.
# For example, to select colour by particle ID:
#/vis/modeling/trajectories/create/drawByParticleID
#/vis/modeling/trajectories/drawByParticleID-0/set e- blue
#
# To superimpose all of the events from a given run:
#/vis/scene/endOfEventAction accumulate
#
# Re-establish auto refreshing and verbosity:
#/vis/viewer/set/autoRefresh true
/vis/verbose warnings
#
# For file-based drivers, use this to create an empty detector view:
#/vis/viewer/flush

/vis/viewer/set/style s
/vis/viewer/set/hiddenEdge 1
/vis/viewer/set/projection p
/vis/viewer/set/background grey

#/vis/viewer/set/background white
#/vis/viewer/set/auxiliaryEdge true

# Rotation style avoiding gimbal lock.
/vis/viewer/set/rotationStyle freeRotation

#
# Viewer menu :
/gui/addMenu   viewer Viewer
#/gui/addButton viewer "My viewer"           "/control/execute vis/myVis.mac"
#/gui/addButton viewer "OIX viewer"          "/control/execute vis/OIX.mac"
#/gui/addButton viewer "X viewer"            "/control/execute vis/X.mac"
#/gui/addButton viewer "Qt viewer"           "/control/execute vis/Qt.mac"
/gui/addButton viewer "Set style solid"     "/vis/viewer/set/style s"
/gui/addButton viewer "Set style wire"      "/vis/viewer/set/style w"
/gui/addButton viewer "Hide tracks behind objects" "/vis/viewer/set/hiddenMarker true"
/gui/addButton viewer "Show tracks behind objects" "/vis/viewer/set/hiddenMarker false"
/gui/addButton viewer "Front view"          "/vis/viewer/set/viewpointThetaPhi 180 0 deg"
/gui/addButton viewer "Back view"           "/vis/viewer/set/viewpointThetaPhi 0 0 deg"
/gui/addButton viewer "Side 1 view"         "/vis/viewer/set/viewpointThetaPhi 90 180 deg"
/gui/addButton viewer "Side 2 view"         "/vis/viewer/set/viewpointThetaPhi -90 180 deg"
/gui/addButton viewer "Top view"            "/vis/viewer/set/viewpointThetaPhi 90 89 deg"
/gui/addButton viewer "Update viewer"       "/vis/viewer/update"
/gui/addButton viewer "Update scene"        "/vis/scene/notifyHandlers"
/gui/addButton viewer "Reset scene"         "/control/execute vis.mac"  # needs path to be set to p348g4/assets/mac
/gui/addButton viewer "Orthogonal proj."    "/vis/viewer/set/projection o"
/gui/addButton viewer "Perspective proj."   "/vis/viewer/set/projection p"

#
# DAWN renderer
/gui/addMenu image Image
/gui/addButton image "DAWN Render"          "/control/execute dawn_render.mac"


# XXX:
# Bird's-eye view of the whole detector components
#  * "/vis/viewer/set/culling global false" makes the invisible 
#    world volume visible.
#    (The invisibility of the world volume is set 
#     in ExN03DetectorConstruction.cc.)
#/vis/viewer/set/style     wireframe
#/vis/viewer/set/culling   global false
#/vis/drawVolume
/vis/scene/add/axes       0 0 0 500 mm
#/vis/scene/add/text       0 0 0 mm 50 -50 -200   world
/vis/scene/add/arrow    0 0 -100 0 0 -80 cm

/vis/viewer/set/viewpointThetaPhi 120 45 deg

#/vis/viewer/set/viewpointThetaPhi 30 60 deg
#/vis/viewer/flush

# XXX:
#/vis/ogl/set/displayListLimit 1000000
#/run/beamOn 5


#/vis/filtering/trajectories/create/particleFilter
#/vis/filtering/trajectories/particleFilter-0/add APrime

#/vis/filtering/trajectories/particleFilter-0/invert true
#/vis/filtering/trajectories/particleFilter-0/verbose true
#/vis/filtering/trajectories/particleFilter-0/active false

#/persistency/gdml/write "/tmp/5.gdml"


/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* This draft searches the characteristic parameters of SADC waveform f(x):
 *  - mean value { <f(x)>, x_mean }
 *  - absolute max value { max(f_i(x_i)), x_i }
 *  - max approximated with bins taken into account by
 *    discrepancy criteria
 *  ...
 *
 *  Gnuplot snippet:
 *  plot '/tmp/one.dat' u 1:2 w boxes title 'Waveform',
 *       '/tmp/one.dat' u 1:3 w linespoints title 'WF deriv.',
 *       '/tmp/one.dat' u 1:4 w lines title 'Linearity'
 *
 * Note that linearity can be used further as correction weights during
 * minimization of fitting functionals. Linearity criteria turns to 1.
 * when the point is near to linear approximation of its two (TODO:?)
 * neighbours.
 */

# include <stdio.h>
# include <stdlib.h>
# include <stdint.h>
# include <string.h>
# include <assert.h>
# include <math.h>

typedef float Samples;
typedef uint8_t SamplesSize;

static const Samples
srcSamples1[] = {
     0, 0, -4, -1,          -2, -1, -5, -1,
     -3, -1, -5, -1,        -4, -3, -6, 33,
     188, 508, 877, 1138,   1158, 1085, 931, 783,
     620, 489, 371, 284,    211, 163, 121, 97
},
srcSamples2[] = {
     0, 0, 0, -2,   0, -2, 2, 1,
     1, 1, 2, -1,   3,  1, 1, 59,
     355, 844, 1382, 1790,   1900, 1758, 1547, 1285,
     1026, 801, 617, 463,    346,   255, 189,  142
},
srcSamples3[] = {
    0.25, 0.25, 0.25, 0.25,         0.25, 0.25, -0.75, -0.75,
    0.25, 1.25, 0.25, 0.25,         1.25, 0.25, 0.25, 1.25,
    1.25, 4.25, 21.25, 63.25,       67.25, 51.25, 69.25, 92.25,
    138.25, 192.25, 222.25, 225.25, 221.25, 208.25, 181.25, 155.2
},
srcSamples4[] = { /* Interesting one -- pile-up with wrong selected pedestals (low amp.) */
    0.5, 0.5, -0.5, -0.5,       -5.5, -1.5, -9.5, -2.5,
    -11.5, -4.5, -9.5, -1.5,    -9.5, -0.5, -5.5, 3.5,
    -5.5, 4.5, -0.5, 6.5,       0.5,  6.5, 1.5, 7.5,
    -0.5, 6.5, 0.5, 7.5,        0.5, 4.5, -0.5, 5.5
},
srcSamples5[] = {
    0, 0, 2, 0,             0, 0, 1, 0,
    0, -1, -1, 0,           0, -2, 1, 0,
    -1, 0, 4, 6,            18, 32, 45, 59,
    78, 99, 125, 153,       171, 180, 182, 175
}
;

struct SADCWFCache {
    Samples * derivatives;
    float * linearity;
};

struct SADCWFCharacteristicParameters {
    SamplesSize absMaxNBin;
    Samples absMaxVal;
    float mean,
          weightedMean,
          sum,
          stdDeviation,
          reliability,
          timeDispersion;
    

    struct SADCWFCache * cache;
};

void
allocate_SADCWF_cache( struct SADCWFCharacteristicParameters * p, SamplesSize nnodes ) {
    p->cache = malloc( sizeof(struct SADCWFCache) );
    bzero( p->cache, sizeof(struct SADCWFCache) );
    p->cache->derivatives = malloc( sizeof(Samples)*(nnodes-1) );
    p->cache->linearity = malloc( sizeof(float)*(nnodes) );
}

void
free_SADCWF_cache( struct SADCWFCharacteristicParameters * p, SamplesSize nnodes ) {
    free(p->cache->derivatives);
    free(p->cache);
}

void
calculate_characteristics( const Samples * samples,
                           const SamplesSize nSamples,
                           struct SADCWFCharacteristicParameters * p ) {
    assert( p->cache );
    uint8_t i, nextIdx, prevIdx, wMIdx;
    float deviation;
    double linearityFactor = 0., integralLinearityCoeff = 0.;
    /* Step #1 : calculate sum and derivative */
    p->sum = 0.;
    p->absMaxVal = samples[0];
    p->absMaxNBin = 0;
    p->timeDispersion = 0.;
    for( i = 0; i < nSamples; i++ ) {
        p->sum += samples[i];
        if(!i) continue;  /* do not eval further code for first bin */
        p->cache->derivatives[i-1] = samples[i] - samples[i-1];
        if( p->absMaxVal < samples[i] ) {
            p->absMaxVal = samples[i];
            p->absMaxNBin = i;
        }
    }
    p->mean = p->sum/nSamples;
    /* Step #2 : calculate weighted mean and linearity */
    p->weightedMean = 0.;
    p->stdDeviation = 0.;
    for( i = 0; i < nSamples; i++ ) {
        p->weightedMean += (samples[i] / p->sum)*i;
        deviation = samples[i] - p->mean;
        p->stdDeviation += deviation*deviation;
        prevIdx = i - 1;
        nextIdx = i + 1;
        if( nextIdx == nSamples ) {
            p->cache->linearity[i] = (1 - fabs(fabs(2*samples[nSamples-2] - samples[nSamples-3]) - samples[i])
                                                / (p->absMaxVal)/2);
        } else if( !i ) {
            p->cache->linearity[i] = (1 - fabs(fabs(2*samples[1] - samples[2]) - samples[i])
                                                / (p->absMaxVal)/2);
        } else {
            p->cache->linearity[i] = (1 - fabs(fabs(samples[prevIdx] + samples[nextIdx])/2 - samples[i])
                                                / (p->absMaxVal)/2);
        }
        
        assert( p->cache->linearity[i] >= 0. );
        assert( p->cache->linearity[i] <= 1. );
        linearityFactor += p->cache->linearity[i];
    }
    p->weightedMean /= nSamples-1;
    p->stdDeviation /= nSamples-1;
    linearityFactor /= nSamples;
    p->stdDeviation  = sqrt(p->stdDeviation);
    wMIdx = (uint8_t) (nSamples*p->weightedMean);
    //printf("$%d\n", (int) wMIdx);
    if(wMIdx >= nSamples) { wMIdx = nSamples - 1; }
    /* Normalize linearity / calc weighted time dispersion */
    for( i = 0; i < nSamples; i++ ) {
        p->timeDispersion = pow( p->weightedMean - i, 2 )*samples[i]/samples[wMIdx];
        //printf( "%e\n", p->timeDispersion );
        p->cache->linearity[i] *= p->cache->linearity[i];
        integralLinearityCoeff += p->cache->linearity[i];
    }
    # if 1
    p->timeDispersion = sqrt(p->timeDispersion);
    p->timeDispersion /= nSamples;
    # endif
    p->reliability = 1 - nSamples/(p->absMaxVal - p->weightedMean*nSamples);
    p->reliability *= integralLinearityCoeff/nSamples;
}

int
main(int argc, char * argv[]) {
    # define tSamples srcSamples3
    const SamplesSize nSamples = sizeof(tSamples)/sizeof(Samples);
    SamplesSize i;
    struct SADCWFCharacteristicParameters p;
    allocate_SADCWF_cache( &p, nSamples );
    calculate_characteristics(
            tSamples,
            nSamples,
            &p );

    # if 1
    for( i = 0; i < nSamples; i++ ) {
        printf( "%d %e %e %e\n",
                (int) i,
                tSamples[i],
                (i ? p.cache->derivatives[i-1] : 0),
                p.cache->linearity[i]*p.absMaxVal );
    }
    printf( "# abs. maximum #bin .... %d\n", (int) p.absMaxNBin );
    printf( "# mean ................. %e\n", p.mean );
    printf( "# weighted mean ........ %e\n", p.weightedMean*nSamples );
    printf( "# sum .................. %e\n", p.sum );
    printf( "# standard deviation ... %e\n", p.stdDeviation );
    printf( "# max def reliability .. %e\n", p.reliability );
    printf( "# weighted time disp. .. %e\n", p.timeDispersion );
    # endif

    free_SADCWF_cache( &p, nSamples );

    return EXIT_SUCCESS;
}


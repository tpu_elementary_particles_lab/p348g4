/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**@brief A boost multimap example.
 * @file boost_multiindex.cpp
 *
 * Using Boost nultimap facility for indexing cross-sections
 * rather than implementing own hashing database looks like
 * better aaproach.
 *
 * This draft is devoted to snippets development in borders of
 * such a task.
 */

# define STANDALONE_BUILD

# if !defined(NDEBUG)
# define BOOST_MULTI_INDEX_ENABLE_INVARIANT_CHECKING
# define BOOST_MULTI_INDEX_ENABLE_SAFE_MODE
# endif
# include <boost/multi_index_container.hpp>
# include <boost/multi_index/ordered_index.hpp>
# include <boost/multi_index/ranked_index.hpp>
# include <boost/multi_index/member.hpp>
# include <boost/multi_index/random_access_index.hpp>
# include <boost/multi_index/composite_key.hpp>
# include <set>
namespace bmi = boost::multi_index;

# include "goo_exception.hpp"

# ifdef STANDALONE_BUILD
#   include <cstdlib>
#   include <iostream>
# else
#   include "whatever"
# endif

# ifdef STANDALONE_BUILD
struct APrimeGenerator {
    double q, m, E;
    uint8_t Z;
    APrimeGenerator( double q_, double m_, double E_, uint8_t Z_ ) :
            q(q_), m(m_), E(E_), Z(Z_) { }
};  // dummy
# endif  // STANDALONE_BUILD

/**@brief Singleton caches storage for A' cross-sections.
 * @class APrimeCSCaches
 *
 * This class implements a dynamic caching storage of pre-computed
 * cross-section values indexed by a subset of parameters. This class
 * utilizes routines from evGen/ directory.
 *
 * The following assumptions are taken into account:
 *  - projectile energy is a continious real value while projectile charge,
 *    mass and nucleus Z will be restricted by particle and material sorts
 *    sets available during application run.
 *  - is must be available to user code to adjust the tabulation ranges of
 *    continious value for fine tuning.
 *
 * This class can sometimes require a significant amount of memory, if there
 * are a lot of different charged projectiles and materials are available in
 * model. However, most of the charged particles in model are rare and can
 * never be involved in run, so by applying lazy cache initialization one
 * can avoid most of the pretty useless data.
 */
class APrimeCSCaches {
public:
    /**@brief Aux class implementing single generator entry.
     * @class Entry
     *
     * This entry class stores generator pointer (unique) and the subset
     * of corresponding parameters: charge and mass of a projectile particle
     * and charge number of target nuclei.
     * */
    struct Entry {
        APrimeGenerator * generator;  ///< A pointer to generator instance
        double charge,  ///< Projectile charge.
               mass,  ///< Projectile mass.
               incE;  ///< Projectile energy.
        uint8_t Z;  /// Target nuclei charge number.

        /// Ctr. Takes a pointer to created generator instance.
        Entry( APrimeGenerator * g, double q, double m, uint8_t mZ, double incidentE ) :
            generator(g), charge(q), mass(m), incE(incidentE), Z(mZ) {}
        friend std::ostream & operator<<( std::ostream & os, const Entry & f ) {
            os << "{q=" << f.charge << ", m_proj=" << f.mass << ", Z_mat=" << (int) f.Z
               << ", E_proj=" << f.incE
               << ", genPtr=" << std::hex << f.generator << "}"
               << std::endl
               ;
            return os;
        }
    };

    /// Cache container type itself.
    typedef boost::multi_index_container<
        Entry,
        bmi::indexed_by<
            bmi::ordered_non_unique<
                bmi::composite_key<
                    Entry,
                    bmi::member<Entry, double,  &Entry::charge>,
                    bmi::member<Entry, double,  &Entry::mass>,
                    bmi::member<Entry, double,  &Entry::incE>,
                    bmi::member<Entry, uint8_t, &Entry::Z>
                >
            >
        >
    > CacheContainer;

private:
    static APrimeCSCaches * _self;
    std::set<double> _projectileERanges;
    CacheContainer _caches;

    APrimeCSCaches();
protected:
    static APrimeGenerator * _new_generator( double q, double m, double E, uint8_t Z );
    static void _delete_generator( APrimeGenerator * );
    void _clear();
public:
    ///
    static APrimeCSCaches & self();

    /**@brief A getter for suitable generator instance.
     *
     * Search among cached data for most suitable pre-computed
     * generator or create one if there is not.
     *
     * @param incidentCharge projectile charge.
     * @param incidentMass projectile mass.
     * @param incidentE projectile kinetic energy.
     * @param nucleusZ Z-number of an element.
     * */
    const APrimeGenerator * generator(
                            double incidentCharge,
                            double incidentMass,
                            double incidentE,
                            uint8_t nucleusZ);

    /**@brief Cross-section value getter.
     *
     * Uses a generator() getter to obtain cached generator instance
     * and returns its total CS value.
     *
     * @param incidentCharge projectile charge.
     * @param incidentMass projectile mass.
     * @param incidentE projectile kinetic energy.
     * @param nucleusZ Z-number of an element.
     * */
    double aprime_total_cross_section_for(
                            double incidentCharge,
                            double incidentMass,
                            double incidentE,
                            uint8_t nucleusZ );

    /// Tabulation setter --- clears all the caches.
    void projectile_energy_tabulation( const std::set<double> & newSet ) {
        _clear();
        _projectileERanges = newSet;
    }

    /// Tabulation getter.
    const std::set<double> & projectile_energy_tabulation() const {
        return _projectileERanges;
    }

    /// Returns number of generators allocated.
    size_t n_generators() const { return _caches.size(); }

    /// Returns a negative integer, if 
    static int test_indexing_routines( int seed=130, size_t nIterations=1e4, std::ostream & os=std::cout );
};


//
//
//

APrimeCSCaches * APrimeCSCaches::_self = nullptr;  // XXX

APrimeCSCaches & APrimeCSCaches::self() {
    if( APrimeCSCaches::_self ) {
        return *APrimeCSCaches::_self;
    }
    return *( _self = new APrimeCSCaches() );
}

APrimeCSCaches::APrimeCSCaches() {}

void
APrimeCSCaches::_clear() {
    // Delete generator pointers from caches:
    for( auto it = _caches.begin(); _caches.end() != it; ++it ) {
        _delete_generator( it->generator );
    }
    _caches.clear();
    _projectileERanges.clear();
}

APrimeGenerator *
APrimeCSCaches::_new_generator( double q, double m, double E, uint8_t Z ) {
    return new APrimeGenerator(q, m, E, Z);  // TODO
}

void
APrimeCSCaches::_delete_generator( APrimeGenerator * g ) {
    delete g;  // TODO
}

const APrimeGenerator *
APrimeCSCaches::generator(
                            double incidentCharge,
                            double incidentMass,
                            double incidentE,
                            uint8_t nucleusZ ) {
    # ifndef NDEBUG
    if( !incidentCharge || std::isnan( incidentCharge )
     || !incidentMass   || std::isnan( incidentMass )
     || !incidentE      || std::isnan( incidentE )
     || !nucleusZ       || std::isnan( nucleusZ ) ) {
        emraise( /*assertFailed*/ badState, "Bad conditions for APrimeCSCaches: q=%e, m=%e,"
                 " E_proj=%e, Z_mat=%u.", incidentCharge, incidentMass,
                 incidentE, nucleusZ );
    }
    # endif

    // Find nearest tabulation boundary:
    double nearestProjE; {
        std::set<double>::const_iterator itUp = _projectileERanges.upper_bound( incidentE );
        # ifndef NDEBUG
        if( _projectileERanges.end() == itUp ) {
            emraise( /*assertFailed*/ badState, "Projectile energy E_proj=%e>%e "
                 "lays above of expected range.", incidentE, *_projectileERanges.crbegin() );
        }
        if( _projectileERanges.begin() == itUp ) {
            emraise( /*assertFailed*/ badState, "Projectile energy E_proj=%e<%e "
                 "lays below of expected range.", incidentE, *_projectileERanges.cbegin() );
        }
        # endif
        std::set<double>::const_iterator itLow = itUp; --itLow;
        const double lowDistance = incidentE - *itLow,
                     upDistance  = *itUp - incidentE;
        nearestProjE = lowDistance > upDistance ? *itUp : * itLow;
    }

    CacheContainer::const_iterator it = _caches.find(
            std::make_tuple( incidentCharge, incidentMass, nearestProjE, nucleusZ ) );
    if( _caches.end() == it ) {
        auto insertionResult = _caches.insert({
                _new_generator( incidentCharge, incidentMass, nearestProjE, nucleusZ ),
                                incidentCharge, incidentMass, nucleusZ, nearestProjE
            });
        if( !insertionResult.second ) {
            emraise( badArchitect, "Element insertion failure! "
                     "Something went wrong with caches look-up procedures "
                     "when existing generator was searched with conditions: "
                     "q=%e, m=%e, E_proj=%e, Z_mat=%u",
                     incidentCharge, incidentMass, incidentE, nucleusZ );
        }
        it = insertionResult.first;
    }
    return it->generator;
}

//
//
//

int
APrimeCSCaches::test_indexing_routines( int seed, size_t nIterations, std::ostream & os ) {
    srand( seed );
    // Note: energyTabulation must be sorted!
    const double energyTabulation[] = { 50, 100, 200, 400, 800, 1600 };
    const double charges[] = { 1, -1 },
                 masses[] = { 1, 2, 3, 5 }
                 ;
    const uint8_t Zs[] = { 1, 15, 26 }
                 ;

    // Set tabulation ranges:
    APrimeCSCaches::self().projectile_energy_tabulation(
            std::set<double>( energyTabulation,
                              energyTabulation + sizeof(energyTabulation)/sizeof(energyTabulation[0]) ) );

    // Run generators acuizition:
    os << "In order to test caching mechanics, run log will "
          "be performed following graph notation:" << std::endl
       << "    \"+\\n\" means that new generator created for some conditions;" << std::endl
       << "    \".\" means that existing generator is obtained for usage." << std::endl
       << "It is expected that one will observe decreasing number "
          "of \"+\"'es up to the finish of the run."
       << std::endl;
    std::set<const APrimeGenerator *> uniqGenerators;
    # define array_size( array ) ( sizeof(array)/sizeof(array[0]) )
    # define random_element( array ) \
    ( array[ (size_t) std::round( (double(rand())/RAND_MAX) * (array_size(array) - 1) ) ] )
    for( size_t n = 0; n < nIterations; ++n ) {
        double charge,
               mass,
               E;
        uint8_t Z;
        auto g = APrimeCSCaches::self().generator(
                        charge = random_element( charges ),
                        mass = random_element( masses ),
                        E = *APrimeCSCaches::self().projectile_energy_tabulation().cbegin() +
                        (double(rand())/RAND_MAX)*(
                            *APrimeCSCaches::self().projectile_energy_tabulation().crbegin()
                            - *APrimeCSCaches::self().projectile_energy_tabulation().cbegin()),
                        Z = random_element( Zs ) );
        if( g->q != charge ) { return -1; }
        if( g->m != mass ) {   return -2; }
        if( g->Z != Z ) {      return -3; }
        { //assert( g.E == E ); --- find nearest E_inc:
            const double * lowE = std::lower_bound( energyTabulation,
                                                    energyTabulation + array_size(energyTabulation),
                                                    E );
            assert( *lowE >= E );  // by the sense of std::lower_bound(...)
            const double downDistance = std::fabs( *lowE - E );
            if( lowE == energyTabulation + array_size(energyTabulation) ) {
                return -4;  // Actually, means that energy was generated wrong.
            }
            if( lowE == energyTabulation && *energyTabulation != E ) {
                return -5;   // Actually, means that energy was generated wrong.
            }
            if( *energyTabulation != E ) {  // otherwise almost impossible, however...
                const double upDistance = std::fabs( *(lowE - 1) - E );
                double nearest;
                if( upDistance > downDistance ) {
                    nearest = *lowE;
                } else {
                    nearest = *(lowE - 1);
                }
                if( nearest != g->E ) {
                    return -6;
                }
            }
        }
        if( uniqGenerators.find( g ) == uniqGenerators.end() ) {
            os << "+" << std::endl;
            uniqGenerators.insert(g);
        } else {
            os << ".";
        }
    }
    os << std::endl << "Done!" << std::endl;
    # undef random_element
    {
        size_t maxGenerators = array_size( energyTabulation ) *
                               array_size( charges ) *
                               array_size( masses ) *
                               array_size( Zs )
                               ;
        os << "Generators created during run: "
                  << APrimeCSCaches::self().n_generators() << std::endl;
        os << "    while at most "
                  << maxGenerators << " generators could be created." << std::endl;
        return maxGenerators - APrimeCSCaches::self().n_generators();
    }
    # undef array_size
}

# ifdef STANDALONE_BUILD
int
main( int argc, char * argv[] ) {
    int rc;
    if( (rc = APrimeCSCaches::test_indexing_routines()) != 0 ) {
        if( rc < 0 ) {
            std::cerr << "Something wrong (" << rc << ")." << std::endl;
            return EXIT_FAILURE;
        }
        std::cerr << "Generators missed: " << rc << std::endl;
    }
    return EXIT_SUCCESS;
}
# endif


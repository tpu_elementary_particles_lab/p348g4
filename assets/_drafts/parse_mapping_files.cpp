# include <regex>
# include <iostream>
# include <fstream>
# include <cstdlib>
# include <functional>
# include <unordered_map>
# include <dirent.h>
# include <regex>

class DetectorMapping {
public:
    typedef int DDDCode;
    typedef int DetectorSignature;
    typedef std::unordered_map<DDDCode, DetectorSignature> MappingContainer;
private:
    MappingContainer _mapping;
    std::unordered_map<int, std::string> _parsers;
public:
    DetectorMapping();
    ~DetectorMapping();

    bool parse_mapping_content( MappingContentType, std::ifstream & );

    static void register_chip();
};  // class ProcessMapDir

bool
DetectorMapping::parse_mapping_content( MappingContentType t, std::ifstream & iF ) {
    std::string line;
    size_t nLinesProcessed = 0;
    std::string parserRxStr;
    {
        auto it = _parsers.find( (int) t );
        if( _parsers.end() == it ) {
            parserRxStr = it->second;
        } else {
            return false;
        }
    }
    std::regex rx(parserRxStr);
    while( std::getline(iF, line) ) {
        std::smatch piecesMatch;
        if( ! std::regex_match(line, piecesMatch, rx) ) {
            continue;
        }
        bool productionSucceed = false;
        // TODO
        if( productionSucceed ) {
            ++nLinesProcessed;
        }
    }
    return nLinesProcessed;
}

int
process_files_in_dir( const std::string & inDirPath, std::function<bool(std::ifstream &)> processor ){
	std::ifstream inn;
    int nFilesProcessed = 0;
	DIR * dirPtr;
	struct dirent *entry;
    if( NULL != (dirPtr = opendir(inDirPath.c_str() )) ){
		while( NULL != (entry = readdir(dirPtr)) ) {
			if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 ){
				inn.open(entry->d_name);
                if( processor( inn ) ) {
                    ++nFilesProcessed;
                }
				inn.close();
			}
			closedir( dirPtr );
		}
        return nFilesProcessed;
	} else {
        return -1;
    }
}

int
main( int argc, char * const argv[] ) {

    //process_files( argv[0] );

    return EXIT_SUCCESS;
}


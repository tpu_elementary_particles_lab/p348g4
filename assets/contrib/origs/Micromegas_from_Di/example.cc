// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
#include "Chip.h"
//#include "ChipSADC.h"
#include "ChipAPV.h"
#include "RecoAPV.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TTree.h"
#include "TInterpreter.h"
#include "TSystem.h"

// c++
#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

// P348 reco
#include "p348reco.h"

int main(int argc, char *argv[])
{
  void ReconAPV(vector<unsigned int>*,vector<unsigned int>*,vector<string>*,vector<vector<short>>*,double&,double&,double&,double&,double&,double&,double&,double&,bool,bool&,bool&,bool&,bool&,bool&,bool&,bool&,bool&);
  void ReadTree();
    
  vector<unsigned int>* srsChip = new vector<unsigned int>;
  vector<unsigned int>* srsChan = new vector<unsigned int>;
  vector<string>* mmChamber = new vector<string>;
  vector<vector<short> >* raw_q = new vector<vector<short> >;
    
  if (argc != 2) {
    cerr << "Usage: ./example <data file name>" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps/2015.xml");
  manager.AddDataSource(argv[1]);
  manager.Print();
  
  #if !defined(__CINT__)
  if (!(gInterpreter->IsLoaded("vector")))
    gInterpreter->ProcessLine("#include <vector>");
  gSystem->Exec("rm -f AutoDict*vector*vector*short*");
  gInterpreter->GenerateDictionary("vector<vector<short> >", "vector");
  //gInterpreter->GenerateDictionary("std::vector<std::vector<short> >", "vector");
#endif
  
  Calibration_init();
  ReadTree();
  
  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  TH1I bgoplot("bgoplot", "BGO;Energy, MeV;#nevents", 150, 0, 150);
  TH1D* momentum = new TH1D("momentum","Reconstructed momentum for electron;Energy(GeV);Entries",2000,0,1000);
  TH1D* cluster1X = new TH1D("cluster1X","X position of beam on MM1;x(mm)",1000,-100,100);
  TH1D* cluster1Y = new TH1D("cluster1Y","Y position of beam on MM1;y(mm)",1000,-100,100);
  TH1D* cluster2X = new TH1D("cluster2X","X position of beam on MM2;x(mm)",1000,-100,100);
  TH1D* cluster2Y = new TH1D("cluster2Y","Y position of beam on MM2;y(mm)",1000,-100,100);
  TH1D* cluster3X = new TH1D("cluster3X","X position of beam on MM3;x(mm)",1000,-100,100);
  TH1D* cluster3Y = new TH1D("cluster3Y","Y position of beam on MM3;y(mm)",1000,-100,100);
  TH1D* cluster4X = new TH1D("cluster4X","X position of beam on MM4;x(mm)",1000,-100,100);
  TH1D* cluster4Y = new TH1D("cluster4Y","Y position of beam on MM4;y(mm)",1000,-100,100);
  TH2D* beam1 = new TH2D("beam1","Beam Profile on MM1;x(mm);y(mm)",1000,-100,100,1000,-100,100);
  TH2D* beam2 = new TH2D("beam2","Beam Profile on MM2;x(mm);y(mm)",1000,-100,100,1000,-100,100);
  TH2D* beam3 = new TH2D("beam3","Beam Profile on MM3;x(mm);y(mm)",1000,-100,100,1000,-100,100);
  TH2D* beam4 = new TH2D("beam4","Beam Profile on MM4;x(mm);y(mm)",1000,-100,100,1000,-100,100);
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // skip auxialy events
    const DaqEvent::EventType type = manager.GetEvent().GetType();
    if (type != DaqEvent::PHYSICS_EVENT) continue;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
    // process only "physics" events (no random or calibration trigger)
    if (!e.isPhysics()) continue;
    
    typedef Chip::Digits::const_iterator Digi_it;
    for (Digi_it i = manager.GetEventDigits().begin(); i != manager.GetEventDigits().end(); ++i ) {
      const Chip::Digit* d = i->second;
      //d.Print();
      
      // skip data from non-APV detectors
      const ChipAPV::Digit* apv = dynamic_cast<const ChipAPV::Digit*>(d);
      
      if (apv == 0) continue;
      
      // description of ChipAPV::Digit:
      // https://svnweb.cern.ch/trac/coral/browser/trunk/src/DaqDataDecoding/src/ChipAPV.h#L50
      
      //apv.Print();
      
      
      // APV readout have 3 samples
      const string& detector = apv->GetDetID().GetName();
      const unsigned int chip = apv->GetChip();
      const unsigned int channel = apv->GetChipChannel();
      const uint32* A = apv->GetAmplitude();
      
      
      srsChip->push_back(chip);
      srsChan->push_back(channel);
      mmChamber->push_back(detector);
      vector<short> chan_data(3);
      chan_data[0] = A[0];
      chan_data[1] = A[1];
      chan_data[2] = A[2];
      raw_q->push_back(chan_data);
      
      /*cout << "detector=" << apv->GetDetID().GetName()
           << " chip=" << apv->GetChip()
           << " channel=" << apv->GetChipChannel()
           << " amplitude[]=" << A[0] << " " << A[1] << " " << A[2]
           << endl;*/
           
    }
    x1_hit=x2_hit=x3_hit=x4_hit=false;
    y1_hit=y2_hit=y3_hit=y4_hit=false;
            
    ReconAPV(srsChip,srsChan,mmChamber,raw_q,x1_pos,y1_pos,x2_pos,y2_pos,x3_pos,y3_pos,x4_pos,y4_pos,isMaxcluster_charge,x1_hit,y1_hit,x2_hit,y2_hit,x3_hit,y3_hit,x4_hit,y4_hit);
    
    double ecal = 0;
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          ecal += e.ECAL[d][x][y].energy;
    
    double hcal = 0;
    for (int d = 0; d < 4; ++d)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y)
          hcal += e.HCAL[d][x][y].energy;
    
    double bgo = 0;
    for (int x = 0; x < 8; ++x)
      bgo += e.BGO[x].energy;
    
    cout << "Event #" << nevt
         << " ECAL = " << ecal << " GeV"
         << " HCAL = " << hcal << " GeV"
         << " BGO = " << bgo << " MeV"
         << endl;
         
         if(x1_hit && y1_hit)
       {
         cluster1X->Fill(x1_pos);
         cluster1Y->Fill(y1_pos);
         beam1->Fill(x1_pos,y1_pos);
         mm1_hit=true;
       }
         if(x2_hit && y2_hit)
       {
         cluster2X->Fill(x2_pos);
         cluster2Y->Fill(y2_pos);
         beam2->Fill(x2_pos,y2_pos);
         mm2_hit=true;
       }
     if(x3_hit && y3_hit)
       {
         cluster3X->Fill(x3_pos);
         cluster3Y->Fill(y3_pos);
         beam3->Fill(x3_pos,y3_pos);
         mm3_hit=true;
       }
     if(x4_hit && y4_hit)
       {
         cluster4X->Fill(x4_pos);
         cluster4Y->Fill(y4_pos);
         beam4->Fill(x4_pos,y4_pos);
         mm4_hit=true;
       }
         
         /*if(mm1_hit && mm2_hit && mm3_hit && mm4_hit)
         {
      def_1X= x_pos[1]-x_pos[0];
          def_2X= x_pos[2]-x_pos[0];
          def_3X= x_pos[3]-x_pos[0];
          def_1Y= y_pos[1]-y_pos[0];
          def_2Y= y_pos[2]-y_pos[0];
          def_3Y= y_pos[3]-y_pos[0];

          def_1 = sqrt(pow(def_1X,2)+pow(def_1Y,2))*1.E-3;
          def_2 = sqrt(pow(def_2X,2)+pow(def_2Y,2))*1.E-3;
          def_3 = sqrt(pow(def_3X,2)+pow(def_3Y,2))*1.E-3;

          rad_1 = sqrt(pow(mag_l,2)+pow(((mag_l*mm43_dist)/(def_3-def_2)),2));

          //rad_mean = ((rad_1+rad_2)/2);


        mom_1 = (q*Bfield*rad_1)/(5.34E-19);
        //mom_2 = (q*Bfield*rad_2)/(5.34E-19);
        //mom_3 = (q*Bfield*rad_3)/(5.34E-19);
        
        //if(std::abs(mom_1-mom_2)<=5 && std::abs(mom_2-mom_3)<=5)
        //{
                mom = mom_1;
                
                cout << "Event #" << nevt
            << " ECAL = " << ecal << " GeV"
            << " HCAL = " << hcal << " GeV"
            << " BGO = " << bgo << " MeV"
            << " Momentum= "<< mom <<" GeV"
            << endl;
            
            momentum->Fill(mom);
        }
        }*/
                
        
        
    ehplot.Fill(ecal, hcal);
    bgoplot.Fill(bgo);
    
    if (nevt > 40000) break;
  }
  
  TCanvas c("plots", "plots");
  c.Print("plots.pdf[");
  
  ehplot.Draw("LEGO 0 FB");
  c.Print("plots.pdf");
  
  c.Clear();
  bgoplot.Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  cluster1X->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  cluster1Y->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  cluster2X->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  cluster2Y->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  cluster3X->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  cluster3Y->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  cluster4X->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  cluster4Y->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  beam1->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  beam2->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  beam3->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  beam4->Draw();
  c.Print("plots.pdf");
  
  c.Clear();
  momentum->Draw();
  c.Print("plots.pdf");
  
  c.Print("plots.pdf]");
  
  return 0;
}
void ReadTree()
{
    TFile* file = new TFile("Mapping_Tree.root","OPEN");
    TTree* tree = (TTree*)file->Get("t1");
    
    unsigned int MM_Ch;
    unsigned int MM_strip;
    unsigned int MM_Layer;
    
    TBranch* b_MM_Ch;
    TBranch* b_MM_strip;
    TBranch* b_MM_Layer;

    tree->SetBranchAddress("MM_Ch",&MM_Ch,&b_MM_Ch);
    tree->SetBranchAddress("MM_strip",&MM_strip,&b_MM_strip);
    tree->SetBranchAddress("MM_Layer",&MM_Layer,&b_MM_Layer);
    
    for(Long64_t tree_entry=0;tree_entry<tree->GetEntriesFast();tree_entry++)
    {
        tree->GetEntry(tree_entry);
        mmmap[MM_Ch].push_back(MM_strip);
    }
    return;
}
void ReconAPV(vector<unsigned int>* srsChip,vector<unsigned int>* srsChan,vector<string>* mmChamber,vector<vector<short>>* raw_q, double& x1_pos, double& y1_pos, double& x2_pos, double& y2_pos, double& x3_pos, double& y3_pos, double& x4_pos, double& y4_pos, bool isMaxcluster_charge, bool& x1_hit, bool& y1_hit, bool& x2_hit, bool& y2_hit, bool& x3_hit, bool& y3_hit, bool& x4_hit, bool& y4_hit)
{

  void DoHitAccumulation(int,vector<unsigned int>*,vector<vector<short>>*,double*,double*);
  void DoMMClustering(double*,bool&,int&,bool);
  void CalculatePos(int,int,double&,double&);
        
    for(int entry=0;entry<srsChip->size();entry++)
    {
        
     if(mmChamber->at(entry)=="MM01") DoHitAccumulation(entry,srsChan,raw_q,MM1_X,MM1_Y); 
     if(mmChamber->at(entry)=="MM02") DoHitAccumulation(entry,srsChan,raw_q,MM2_X,MM2_Y); 
     if(mmChamber->at(entry)=="MM03") DoHitAccumulation(entry,srsChan,raw_q,MM3_X,MM3_Y);
     if(mmChamber->at(entry)=="MM04") DoHitAccumulation(entry,srsChan,raw_q,MM4_X,MM4_Y);  
    }    
    
    for(int array_size=0;array_size<MMSize;array_size++)
        {
            if(MM1_X[array_size]!=0) mmx1_hit=true;
            if(MM1_Y[array_size]!=0) mmy1_hit=true;
            if(MM2_X[array_size]!=0) mmx2_hit=true;
            if(MM2_Y[array_size]!=0) mmy2_hit=true;
            if(MM3_X[array_size]!=0) mmx3_hit=true;
            if(MM3_Y[array_size]!=0) mmy3_hit=true;
            if(MM4_X[array_size]!=0) mmx4_hit=true;
            if(MM4_Y[array_size]!=0) mmy4_hit=true;
        }

    if(mmx1_hit) DoMMClustering(MM1_X,x1_hit,strip_x1_pos,isMaxcluster_charge);
    if(mmy1_hit) DoMMClustering(MM1_Y,y1_hit,strip_y1_pos,isMaxcluster_charge);
    if(mmx2_hit) DoMMClustering(MM2_X,x2_hit,strip_x2_pos,isMaxcluster_charge);
    if(mmy2_hit) DoMMClustering(MM2_Y,y2_hit,strip_y2_pos,isMaxcluster_charge);
    if(mmx3_hit) DoMMClustering(MM3_X,x3_hit,strip_x3_pos,isMaxcluster_charge);
    if(mmy3_hit) DoMMClustering(MM3_Y,y3_hit,strip_y3_pos,isMaxcluster_charge);
    if(mmx4_hit) DoMMClustering(MM4_X,x4_hit,strip_x4_pos,isMaxcluster_charge);
    if(mmy4_hit) DoMMClustering(MM4_Y,y4_hit,strip_y4_pos,isMaxcluster_charge);
    
    for(int array_size=0;array_size<MMSize;array_size++)
    {
        MM1_X[array_size]=0;
        MM1_Y[array_size]=0;
        MM2_X[array_size]=0;
        MM2_Y[array_size]=0;
        MM3_X[array_size]=0;
        MM3_Y[array_size]=0;
        MM4_X[array_size]=0;
        MM4_Y[array_size]=0;
    }
            mmx1_hit=false;
            mmy1_hit=false;
            mmx2_hit=false;
            mmy2_hit=false;
            mmx3_hit=false;
            mmy3_hit=false;
            mmx4_hit=false;
            mmy4_hit=false;


    if(x1_hit && y1_hit) CalculatePos(strip_x1_pos,strip_y1_pos,x1_pos,y1_pos);
    
    if(x2_hit && y2_hit) CalculatePos(strip_x2_pos,strip_y2_pos,x2_pos,y2_pos);
    
    if(x3_hit && y3_hit) CalculatePos(strip_x3_pos,strip_y3_pos,x3_pos,y3_pos);
    
    if(x4_hit && y4_hit) CalculatePos(strip_x4_pos,strip_y4_pos,x4_pos,y4_pos);
    

    
    return;
}

void DoHitAccumulation(int entry,vector<unsigned int>* srsChan,vector<vector<short>>* raw_q, double* MM_X, double* MM_Y)
{
    int chan=0;
    double max_q=0;

    for(int q_sample=0;q_sample<raw_q->at(entry).size();q_sample++)
        {
            if(max_q<raw_q->at(entry).at(q_sample))
            {
                max_q=raw_q->at(entry).at(q_sample);
            }
            chan=srsChan->at(entry);
            
        }
        chan = srsChan->at(entry);
    if(chan%2==0)
    {  
    for(int map_entry=0; map_entry<mmmap[chan+1].size();map_entry++)
        {
        const int strip= mmmap[chan+1][map_entry];
               MM_X[strip]=max_q;
        }
    }
    if((chan+1)%2==0)
    {
        for(int map_entry=0; map_entry<mmmap[chan+1].size();map_entry++)
        {
        const int strip= mmmap[chan+1][map_entry];
                MM_Y[strip]=max_q;
        }
    }
    return;
    
}
void DoMMClustering(double* MM_array,bool& hit,int& strip_pos,bool isMaxcluster_charge)
{
    double bin_n_pos[320][320];
    double charge_n_pos[320][320], charge_total_pos[320];
    double num=0, den=0;
    int size_pos[320];
    int loop1=0, loop2=0, loop1F=0;
    
    
    for(int bin=0;bin<MMSize;bin++)
    {
        if(bin<=(MMSize-1) && (bin+1)<=(MMSize-1) && (bin+2)<=(MMSize-1))
        {
            if(MM_array[bin]!=0 && MM_array[bin+1]!=0 && MM_array[bin+2]!=0)
            {
                for(int loop=0;loop<3;loop++)
                {
                    bin_n_pos[loop1][loop2]=bin+loop;
                    charge_n_pos[loop1][loop2]=MM_array[bin+loop];
                
                    charge_total_pos[loop1]=charge_total_pos[loop1]+MM_array[bin+loop];
                
                //cout<<"Bin "<<loop1<<" "<<loop2<<" "<<bin_n_pos[loop1][loop2]<<endl;
                //getchar();
                    loop2++;
                }
                
                for(int inc=3;inc<(MMSize-3);inc++)
                {
                    int binnew=bin+inc;
                    if(binnew<=(MMSize-1) && MM_array[binnew]!=0)
                    {
                        bin_n_pos[loop1][loop2]=binnew;
                        charge_n_pos[loop1][loop2]=MM_array[binnew];
                        
                        charge_total_pos[loop1]=charge_total_pos[loop1]+MM_array[binnew];
                        
                        //cout<<"Bin "<<loop1<<" "<<loop2<<" "<<bin_n_pos[loop1][loop2]<<endl;
                        //getchar();
                        loop2++;
                    }
                    else if(binnew<=(MMSize-1) && MM_array[binnew]==0)
                    {
                        size_pos[loop1]=loop2-1;
                        //loop2=0;
                        bin=binnew;
                        break;
                    }
                    if(binnew>(MMSize-1))
                    {
                        size_pos[loop1]=loop2-1;
                        bin=binnew;
                        break;
                    }
                }
                if(loop2<315)
                {
                    loop2=0;
                    loop1++;
                }
                else
                {
                    loop2=0;
                    charge_total_pos[loop1]=0;
                    //size_pos[loop1]=0;
                }
            }
        }
    }
    if(loop1>=1)
    {
        int loop1new = loop1;
        if(isMaxcluster_charge)
        {
            for(int inc=0;inc<loop1new;inc++)
            {
                if(charge_total_pos[inc]>charge_total_pos[loop1-1])
                {
                    loop1=inc+1;
                }
                else
                {
                    loop1=loop1;
                }
            }
        }
        else
        {
            for(int inc=0;inc<loop1new;inc++)
            {
                //cout<<"size "<<inc<<size_pos[inc]<<" sizeloop "<<loop1-1<<" "<<size_pos[loop1-1]<<endl;
                //getchar();
                if(size_pos[inc]>size_pos[loop1-1])
                {
                    loop1 = inc+1;
                }
                else
                {
                    loop1 = loop1;
                }
                //cout<<"loop1 "<<loop1<<endl;
            }
        }
        
        for(int inc=0;inc<=size_pos[loop1-1];inc++)
        {
            num+=(bin_n_pos[loop1-1][inc]*charge_n_pos[loop1-1][inc]);
            den+=charge_n_pos[loop1-1][inc];
        }
        
        for(int inc=0;inc<loop1new;inc++)
        {
            charge_total_pos[inc]=0;
            size_pos[inc]=0;
        }
        //if(sizex_pos[loop1-1]%2==0)loop2 = sizex_pos[loop1-1]/2;
        //else loop2 = (sizex_pos[loop1-1]+1)/2;
        //cout<<"loop1 "<<loop1-1<<" loop2 "<<loop2<<endl;
        
        //strip_x=bin_nx_pos[loop1-1][loop2];
        strip_pos=num/den;
        
        hit=true;
        
        //cout<<"Strip X "<<strip_x<<endl;
        //getchar();
        loop1=0;
        loop2=0;
        num=0;
        den=0;
    }
    for(int array_size=0;array_size<MMSize;array_size++)
    {
        MM_array[array_size]=0;
    }
    return;
}

void CalculatePos(int strip_x, int strip_y, double& x_pos, double& y_pos)
{
    x_pos= ((0.25*(160-strip_x))/sqrt(2)-(0.25*(160-strip_y))/sqrt(2));
    y_pos= ((0.25*(160-strip_x))/sqrt(2)+(0.25*(160-strip_y))/sqrt(2));
    return;
}


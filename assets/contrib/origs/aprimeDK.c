#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <time.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_monte_vegas.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_rng.h>

#define Mel 5.1E-04 // electron mass
#define alphaEW 1.0/137.0 
#define MUp 2.79 // protonMu
#define Mpr 0.938 // proton mass
#define max_uint 4294967296.0l
#define GeVtoPb 3.894E+08 // GeV^{-2} to Pbarn


double DifSigmaForMaximum  (const gsl_vector *v, void *params);
double chi (double x, void * params);
struct ParamsForChi {double AA; double ZZ; double MMA; double EE0;};


double DifSigmaForMaximum (const gsl_vector *v, void *params){
  double x, theta;
  double *p = (double *)params;
  
/* Reminder:

   p[0]= A;
   p[1]= Z;
   p[2]= MA;
   p[3]= E0;
   p[4]=epsil;

 this diff sigma without prefactor
*/

  x = gsl_vector_get(v, 0);
  theta = gsl_vector_get(v, 1); 
	double Xminf = p[2]/p[3];
	double Xmaxf = 1.0 -p[2]/p[3]; // X cutoff 
	double ThetaMaxA = pow(p[2]/p[3],1.5);
	double Uxtheta = p[3]*p[3]*theta*theta*x+p[2]*p[2]*(1.0-x)/x+Mel*Mel*x;
	double AA = (1.0-x+x*x/2.0)/Uxtheta/Uxtheta; 
	double BB = (1.0-x)*(1.0-x)*p[2]*p[2]/Uxtheta/Uxtheta/Uxtheta/Uxtheta;
	double CC = p[2]*p[2]-Uxtheta*x/(1.0-x);
	double res = AA+BB*CC;
   
 if ( x<=Xmaxf ) return -res; 
 else return 0;
}



double chi (double t, void * pp) {
  ParamsForChi * params = (ParamsForChi *)pp;

/* Reminder II:
   params->AA;
   params->ZZ;
   params->MMA;
   params->EE0;
*/

 double d = 0.164/pow((params->AA),2/3);
 double ap = 773.0/Mel/pow((params->ZZ),2/3);
 double a = 111.0/Mel/pow((params->ZZ),1/3);
 double G2el = (params->ZZ)*(params->ZZ)*a*a*a*a*t*t/(1.0+a*a*t)/(1.0+a*a*t)/(1.0+t/d)/(1.0+t/d);
 double G2in = (params->ZZ)*ap*ap*ap*ap*t*t/(1.0+ap*ap*t)/(1.0+ap*ap*t)/(1.0+t/0.71)/(1.0+t/0.71)
  /(1.0+t/0.71)/(1.0+t/0.71)/(1.0+t/0.71)/(1.0+t/0.71)/(1.0+t/0.71)/(1.0+t/0.71)
  *(1.0+t*(MUp*MUp-1.0)/4.0/Mpr/Mpr)*(1.0+t*(MUp*MUp-1.0)/4.0/Mpr/Mpr);
 double G2 = G2el+G2in;
 double ttmin = (params->MMA)*(params->MMA)*(params->MMA)*(params->MMA)/4.0/(params->EE0)/(params->EE0); 
// double ttmin = lowerLimit(x,theta,p); 
 double Under = G2*(t-ttmin)/t/t;

return Under;
}

int 
main(void)
{

/* Reminder:
   p[0]= A;
   p[1]= Z;
   p[2]= MA;
   p[3]= E0;
   p[4]=epsil;
*/

double A = 207.0; // Pb atomic mass  
double Z = 84.0; // char
double MA= 0.5; // A' mass 
double E0= 80.0; // energy of electron beam
double epsil = 1.0E-04;
double Xmin = MA/E0;
double Xmax = 1.0-Xmin;  
double ThetaMaxA = pow(MA/E0,1.5);
double ThetaMaxEl = sqrt(MA*Mel)/E0;


//printf ("ThetaMaxA = %e\n", ThetaMaxA);
//printf ("ThetaMaxEl = %e\n", ThetaMaxEl);

  double par[5] = {A, Z, MA, E0, epsil};


//////////////////////////////////////////////////////////////////



//begin: chi-formfactor calculation 

gsl_integration_workspace * w 
    = gsl_integration_workspace_alloc (1000);
  
  double result, error;
 // double expected = -4.0;
  double tmin = par[2]*par[2]*par[2]*par[2]/4.0/par[3]/par[3];
  double tmax = par[2]*par[2];

 
 
  gsl_function F;
  ParamsForChi alpha = {1.0, 1.0, 1.0, 1.0}; 
  F.function = &chi;
  F.params = &alpha;

alpha.AA = A;
alpha.ZZ = Z;
alpha.MMA=MA;
alpha.EE0=E0;


  gsl_integration_qags (&F, tmin, tmax, 0, 1e-7, 1000,
                        w, &result, &error); 

  printf ("chi/Z^2 = % .18f\n", result/Z/Z);
 // printf ("exact result    = % .18f\n", expected);
 // printf ("estimated error = % .18f\n", error);
 // printf ("actual error    = % .18f\n", result - expected);
//  printf ("intervals =  %d\n", w->size);

double ChiRes=result;

  gsl_integration_workspace_free (w);

//end: chi-formfactor calculation 




////////////////////////////////////////////////////////////////////

//begin: SigmaMaxCalculation

double UxthetaMax = par[2]*par[2]*(1.0-Xmax)/Xmax+Mel*Mel*Xmax;
	double AAMax = (1.0-Xmax+Xmax*Xmax/2.0)/UxthetaMax/UxthetaMax; 
	double BBMax = (1.0-Xmax)*(1.0-Xmax)*par[2]*par[2]/UxthetaMax/UxthetaMax/UxthetaMax/UxthetaMax;
	double CCMax = par[2]*par[2]-UxthetaMax*Xmax/(1.0-Xmax);
	double sigmaMax = AAMax+BBMax*CCMax;

//end: SigmaMaxCalculation

////////////////////////////////////////////////////////////////////


//begin: event selection


srand (time(NULL));

int iii=0;
double XAcc, ThetaAcc, PhiAcc;


for( iii= 1; iii<1000; iii++)
{

	double XEv  =  rand()/(RAND_MAX+1.0)*(Xmax-Xmin)+Xmin; 
	double ThetaEv =  rand()/(RAND_MAX+1.0)* ThetaMaxA;  
	double PhiEv = rand()/(RAND_MAX+1.0) * 2.0*3.1415926; 
	double UU = rand()/(RAND_MAX+1.0)*sigmaMax;

//printf( "XEv=%e ThetaEv=%e PhiEv= %e\n", XEv, ThetaEv, PhiEv);


	double Uxtheta = par[3]*par[3]*ThetaEv*ThetaEv*XEv+par[2]*par[2]*(1.0-XEv)/XEv+Mel*Mel*XEv;
	double AA = (1.0-XEv+XEv*XEv/2.0)/Uxtheta/Uxtheta; 
	double BB = (1.0-XEv)*(1.0-XEv)*par[2]*par[2]/Uxtheta/Uxtheta/Uxtheta/Uxtheta;
	double CC = par[2]*par[2]-Uxtheta*XEv/(1.0-XEv);
	double sigma = AA+BB*CC;

		if(sigma>=UU){
		XAcc = XEv;
		ThetaAcc = ThetaEv;
		PhiAcc = PhiEv;	
		printf( "XAcc=%e ThetaAcc=%e PhiAcc= %e\n ", XAcc, ThetaAcc, PhiAcc, sigma, UU);
		break;
	}

}
//end: event selection 
/////////////////////////////////////////////////////


//begin: tottal cross-section in Pbarn. Factor 1/2 by Andreas is taken into account 

double beta = sqrt(1.0-MA*MA/E0/E0);
double SigmaTot= GeVtoPb*4.0/3.0*alphaEW*alphaEW*alphaEW*epsil*epsil*ChiRes*beta*log(E0*E0/MA/MA)/MA/MA;

printf( "SigmaTot (pb)=%e  \n ", SigmaTot);

//end: totat cross-section



return 0;

}

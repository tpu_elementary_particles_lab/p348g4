/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <regex>
# include <iostream>
# include <unordered_map>

static const std::string sepTokensExamples[] = {
    "one",
    "Bad_Camel",
    "${bar}",
    "/one/two",
    "one${foo}two",
    ""
};

static const std::string valExamples[] = {
    "someSDClass:/instanceName",
    "other_SD_123_Class:/other_instance14",
    "anotherSDClass:/templatedInstance${foo}",
    "anotherSDClass:/yetAnother${foo}/Templated/Instance${bar_zum}",
    "ECAL_cell:/p348det/ECALSegment",
    ""
};

static const std::string
    srxSDClass  = "((?:[[:alnum:]_])+)",
    srxVarSubst = "(\\$\\{(?:[[:alnum:]_]+)\\})",
    srxSDName   = "(/(?:[[:alnum:]_/]|" + srxVarSubst + ")+)",
    srxSDID     = "^" + srxSDClass + ":" + srxSDName + "$"
    ;

static const std::regex
    rxSDClass   ( srxSDClass ),
    rxVarSubst  ( srxVarSubst ),
    rxSDName    ( srxSDName ),
    rxSDID      ( srxSDID )
    ;

std::string
substitute_variables( const std::string & orig, std::unordered_map<std::string, std::string> varDict ) {
    std::string res = orig;
    for( auto it = varDict.begin(); varDict.end() != it; ++it ) {
        res = std::regex_replace( res, std::regex("\\$\\{" + it->first + "\\}"), it->second );
    }
    return res;
}

int
main(int rgc, char * argv[]) {
    {
        std::smatch sm;
        const std::regex * regexesToTest[] = { &rxSDClass, &rxVarSubst, &rxSDName, &rxSDName, nullptr };
        const std::string * srxToTest[] = { &srxSDClass, &srxVarSubst, &srxSDName, &srxSDName, nullptr };
        for( const std::string * example = sepTokensExamples;
             !example->empty();
             example++ ) {
            const std::string ** srx = srxToTest;
            for( const std::regex ** crx = regexesToTest;
                 *crx;
                 ++crx, ++srx ) {
                if( std::regex_match( *example, sm, **crx ) ) {
                    std::cout << "example matches: " << *example << " w regex: " << **srx << std::endl;
                } else {
                    std::cout << "       mismatch: " << *example << " w regex: " << **srx << std::endl;
                }
            }
        }
    }

    std::unordered_map<std::string, std::string> variables; {
        variables["foo"]     = "__HereWasAFoo__";
        variables["bar"]     = "__HereWasABar__";
        variables["bar_zum"] = "__HereWasABarZum__";
    }

    std::cout << std::endl << "***" << std::endl;

    for( const std::string * example = valExamples;
         !example->empty();
         example++ ) {
        std::string toProcess = *example;
        bool needSubst = false;
        do {
            if( needSubst ) {
                toProcess = substitute_variables( toProcess, variables );
                needSubst = false;
            }
            std::smatch sm;
            if( std::regex_match( toProcess, sm, rxSDID ) ) {
                for( size_t nPiece = 0;
                     nPiece < sm.size();
                     ++nPiece ) {
                    std::ssub_match subMatch = sm[nPiece];
                    std::string piece = subMatch.str();
                    if(piece.empty()) continue;

                    if( std::regex_match( piece, rxSDID ) ) {
                        std::cout << "Entire matching string: " << piece << std::endl;
                    } else if( std::regex_match( piece, rxVarSubst ) ) {
                        std::cout << "            - Variable: " << piece << std::endl;
                        needSubst = true;
                        std::cout << "== BREAK -- RENEW ==" << std::endl;
                    } else if( std::regex_match( piece, rxSDName ) ) {
                        std::cout << "       - Instance name: " << piece << std::endl;
                    } else if( std::regex_match( piece, rxSDClass ) ) {
                        std::cout << "               - Class: " << piece << std::endl;
                    } else {
                        std::cerr << "ERROR: unmatched \"" << piece << "\"" << std::endl;
                    }
                }
            } else {
                std::cerr << "ERROR: Unmatched example line: \"" << toProcess << "\"" << std::endl;
            }
        } while( needSubst );
    }

    return EXIT_SUCCESS;
}


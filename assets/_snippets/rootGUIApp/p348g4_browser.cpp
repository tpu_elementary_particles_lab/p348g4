/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <climits>

# include "p348g4_browser.hpp"

# include <TGStatusBar.h>
# include <TGCanvas.h>
# include <TGListTree.h>
# include <TGMenu.h>
# include <TGTab.h>

void
ObjectBrowsingFrame::DoSave() {
  Printf("Save in progress...");
  SaveSource("","");
}

void
ObjectBrowsingFrame::_compose_menubar() {
    auto menuBar = new TGMenuBar( this, 5, 5, kHorizontalFrame /*| kRaisedFrame*/ );
    // Menu bar
    auto fMenuFile = new TGPopupMenu( fClient->GetRoot(), 5, 5, kHorizontalFrame );
    fMenuFile->Associate(this);
    // adding menu entries
    fMenuFile->AddEntry("&Open...",         -1);
    fMenuFile->AddEntry("&Save",            -1);
    fMenuFile->AddEntry("S&ave as...",      -1);
    fMenuFile->AddEntry("&Close",           -1);
    // adding separator
    fMenuFile->AddSeparator();
    // next group of menu entries
    fMenuFile->AddEntry("&Print",           -1);
    fMenuFile->AddEntry("P&rint setup...",  -1);
    //. . .
    fMenuFile->AddSeparator();
    fMenuFile->AddEntry("E&xit",            -1);

    menuBar->AddPopup(  "&File",
                        fMenuFile,
                        new TGLayoutHints( kLHintsLeft, 0, 0, 0, 0)
                );
    AddFrame( menuBar, new TGLayoutHints( kLHintsLeft | kLHintsExpandX | kLHintsTop ) );
}

void
ObjectBrowsingFrame::_compose_statusbar() {
    // Status bar
    Int_t parts[] = {45, 15, 10, 30};
    auto fStatusBar = new TGStatusBar(this, 50, 10, kVerticalFrame);
         fStatusBar->SetParts(parts, 4);
         fStatusBar->Draw3DCorner(kTRUE);
    AddFrame(fStatusBar, new TGLayoutHints(kLHintsExpandX | kLHintsBottom, 0, 0, 0, 0));
}

void
ObjectBrowsingFrame::_compose_entity_browser() {
    _desktopFrame = new TGHorizontalFrame(this, 50, 50);
        _browsingFrame = new TGVerticalFrame(_desktopFrame, 100, 10, kFixedWidth);
            _browsingFrameSub = new TGCompositeFrame(_browsingFrame, 100, 10, kSunkenFrame);
        _tabsFrame = new TGVerticalFrame(_desktopFrame, 10, 10);
            _tabsFrameSub = new TGCompositeFrame(_tabsFrame, 100, 10, kSunkenFrame);
    _compose_entity_tree_view();
    _compose_entity_viewport();
    //TGLabel *fLleft = new TGLabel(fFleft, "Left Frame");
    //TGLabel *fLright = new TGLabel(fFright, "Right Frame");
    //fFleft->AddFrame(fLleft, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 
    //                                          3, 0, 0, 0));
    //fFright->AddFrame(fLright, new TGLayoutHints(kLHintsLeft | kLHintsCenterY,
    //                                            3, 0, 0, 0));
    _browsingFrame->AddFrame(_browsingFrameSub, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
    _tabsFrame->AddFrame(_tabsFrameSub, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
    //_browsingFrame->Resize(_browsingFrameSub->GetDefaultWidth()+20, _browsingFrame->GetDefaultHeight());
    _tabsFrame->Resize(_tabsFrameSub->GetDefaultWidth(), _browsingFrame->GetDefaultHeight());
    _desktopFrame->AddFrame(_browsingFrame, new TGLayoutHints(kLHintsLeft | kLHintsExpandY));
    TGVSplitter *splitter = new TGVSplitter(_desktopFrame,2,2);
    splitter->SetFrame(_browsingFrame, kTRUE);
    _desktopFrame->AddFrame(splitter, new TGLayoutHints(kLHintsLeft | kLHintsExpandY));
    _desktopFrame->AddFrame(_tabsFrame, new TGLayoutHints(
                                          kLHintsRight
                                        | kLHintsExpandX
                                        | kLHintsExpandY));
    AddFrame(_desktopFrame, new TGLayoutHints( kLHintsRight
                                             | kLHintsExpandX
                                             | kLHintsExpandY
                                             | kFitHeight ));
}

void
ObjectBrowsingFrame::_compose_entity_tree_view() {
    // canvas widget
    TGCanvas *fCanvas635 = new TGCanvas(_browsingFrameSub, 50, 50);
    fCanvas635->SetName("fCanvas635");
    // canvas viewport
    TGViewPort *fViewPort636 = fCanvas635->GetViewPort();
    // list tree
    TGListTree *fListTree645 = new TGListTree(fCanvas635, kVerticalFrame);
    fListTree645->Associate(this);
    fListTree645->SetName("fListTree645");
    const TGPicture *popen;       //used for list tree items
    const TGPicture *pclose;      //used for list tree items

    TGListTreeItem *item0 = fListTree645->AddItem(NULL,"Entry 1");
    popen = gClient->GetPicture("ofolder_t.xpm");
    pclose = gClient->GetPicture("folder_t.xpm");
    item0->SetPictures(popen, pclose);
    fListTree645->CloseItem(item0);
    TGListTreeItem *item1 = fListTree645->AddItem(NULL,"Entry 2");
    item1->SetPictures(popen, pclose);
    fListTree645->CloseItem(item1);
    TGListTreeItem *item2 = fListTree645->AddItem(NULL,"Entry 3");
    item2->SetPictures(popen, pclose);
    fListTree645->CloseItem(item2);
    TGListTreeItem *item3 = fListTree645->AddItem(NULL,"Entry 4");
    item3->SetPictures(popen, pclose);
    fListTree645->CloseItem(item3);
    TGListTreeItem *item4 = fListTree645->AddItem(NULL,"Entry 5");
    item4->SetPictures(popen, pclose);
    fListTree645->CloseItem(item4);

    fViewPort636->AddFrame(fListTree645, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
    fListTree645->SetLayoutManager(new TGHorizontalLayout(fListTree645));
    fListTree645->MapSubwindows();
    fCanvas635->SetContainer(fListTree645);
    fCanvas635->MapSubwindows();
    _browsingFrameSub->AddFrame(fCanvas635, new TGLayoutHints(
                                      kLHintsLeft
                                    | kLHintsTop
                                    | kLHintsExpandX
                                    | kLHintsExpandY));
    fCanvas635->MoveResize(16,24,152,0);
}

void
ObjectBrowsingFrame::_compose_entity_viewport() {
    // _tabsFrame
    // tab widget
    TGTab *fTab560 = new TGTab( _tabsFrameSub, 50, 50 );

    // container of "Tab1"
    TGCompositeFrame *fCompositeFrame563;
    fCompositeFrame563 = fTab560->AddTab("Tab1");
    fCompositeFrame563->SetLayoutManager(new TGVerticalLayout(fCompositeFrame563));

    // container of "Tab2"
    TGCompositeFrame *fCompositeFrame565;
    fCompositeFrame565 = fTab560->AddTab("Tab2");
    fCompositeFrame565->SetLayoutManager(new TGVerticalLayout(fCompositeFrame565));

    fTab560->SetTab(0);

    fTab560->Resize(fTab560->GetDefaultSize());
    _tabsFrameSub->AddFrame(fTab560, new TGLayoutHints(   kLHintsLeft
                                                        | kLHintsTop
                                                        | kLHintsExpandX
                                                        | kLHintsExpandY ));
    fTab560->MoveResize(8,8,472,360);
}

ObjectBrowsingFrame::ObjectBrowsingFrame(
            const TGWindow *p,
            UInt_t w,
            UInt_t h) :
        TGMainFrame(p, w, h) {
    // Create vertical splitter
    _compose_menubar();
    ///////////////////////////////////////////////////////////////////////////
    _compose_entity_browser();
    // button frame
    //TGVerticalFrame *vframe = new TGVerticalFrame(this, 10, 10);
    //TGCompositeFrame *cframe2 = new TGCompositeFrame(vframe, 170, 20,
    //                                         kHorizontalFrame | kFixedWidth);
    //TGTextButton *save = new TGTextButton(cframe2, "&Save");
    //cframe2->AddFrame(save, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
    //                                         3, 2, 2, 2));
    //save->Connect("Clicked()", "ObjectBrowsingFrame", this, "DoSave()");
    //save->SetToolTipText("Click on the button to save the application as C++ macro");
   
    //TGTextButton *exit = new TGTextButton(cframe2, "&Exit ","gApplication->Terminate(0)");
    //cframe2->AddFrame(exit, new TGLayoutHints(kLHintsTop | kLHintsExpandX,
    //                                         2, 0, 2, 2));
    //vframe->AddFrame(cframe2, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
    //AddFrame(vframe, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
    _compose_statusbar();
    ///////////////////////////////////////////////////////////////////////////
    // What to clean up in destructor
    SetCleanup(kDeepCleanup);
    // Set a name to the main frame   
    SetWindowName("P348g4 events monitor");
    SetWMSizeHints(350, 200, USHRT_MAX, USHRT_MAX, 0, 0);
    MapSubwindows();
    Resize(GetDefaultSize());
    MapWindow();
}


ObjectBrowsingFrame::~ObjectBrowsingFrame() {
    // Clean up all widgets, frames and layouthints that were used
    Cleanup();
}


void
ObjectBrowsingFrame::CloseWindow() {
    // Called when window is closed via the window manager.
    delete this;
}


/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef P348_G4_EV_BRWOSER_ENTRY_H
# define P348_G4_EV_BRWOSER_ENTRY_H

# include <map>

struct Entry {
public:
    typedef void (*Callback)();
    enum TypeCode {
        inactive        = 0,
        callback        = 2,
        canvasPreset    = 3,
        category        = 50,   // <= meaningful number --- all above is
        unknownTObject  = 100,  //    a TObject instances.
        histogram       = 101,
        tfile           = 102,
        // ...
    };
protected:
    TypeCode _tCode;
    union {
        Callback _callback;
        TObject * _objPtr;
    } uData;
    virtual void _V_invoke() = 0;
public:
    Entry();
    virtual ~Entry() {}

    template<typename TObjConcreteType *> TObjConcreteType object_ptr() {
        if( _tCode < 100 ) { return 0; }
        return dynamic_cast<TObjConcreteType *>(uData._objPtr); }

    void invoke() { _V_invoke(); }
};

# endif  // P348_G4_EV_BRWOSER_ENTRY_H

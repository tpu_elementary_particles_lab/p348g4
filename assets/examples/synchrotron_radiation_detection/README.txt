This example is devoted to synchrotron radiation detection task. List of
setup:
    -- SR "shashlyk"-type detector.
    -- Vacuum vessel which is using for decreasing secondary interactions.
    -- Magnet vessel is located inside the magnet, that is why there is
    "magnet" in the name.

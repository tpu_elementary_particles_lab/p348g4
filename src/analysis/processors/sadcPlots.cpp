/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# if defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

# include "app/analysis.hpp"
# include <TH1F.h>
# include <TH2F.h>
# include <TFile.h>
# include <TDirectory.h>
# include <TProfile.h>
# include <TSystem.h>
# include "p348g4_detector_ids.h"
# include "app/cvalidators.hpp"
# include "p348g4_uevent.hpp"
# include "app/analysis_cat_mx.tcc"

namespace p348 {
namespace dprocessors {
namespace aux {

class SADC_WFPlotsCollector : public ::p348::AnalysisApplication::iSADCProcessor,
                              public ::p348::mixins::DetectorCatalogue< std::pair<TProfile *, TH2F *> > {
protected:
    virtual void _V_print_brief_summary( std::ostream & os ) const override;
    virtual bool _V_process_SADC_profile_event( p348::events::SADC_profile * ) override;
    std::pair<TProfile *, TH2F *> _V_new_entry( DetectorSignature id, TDirectory * ) override;
    void _V_free_entry( DetectorSignature, std::pair<TProfile *, TH2F *> ) override;

    const p348::aux::HistogramParameters2D _sadcHstPars;
    bool _doCollectStats;
public:
    SADC_WFPlotsCollector(
                    const p348::aux::HistogramParameters2D & sadcHstPars,
                    const std::string & dirName="SADCProfiles" ) :
                ::p348::mixins::DetectorCatalogue< std::pair<TProfile *, TH2F *> >(dirName),
                _sadcHstPars(sadcHstPars),
                _doCollectStats(sadcHstPars.nBins[0]) {}
};


std::pair<TProfile *, TH2F *>
SADC_WFPlotsCollector::_V_new_entry( DetectorSignature id, TDirectory * ) {
    std::pair<TProfile *, TH2F *> res;
    {
        UniqueDetectorID uID(id);
        char detNamebf[32], namebf[64], labelbf[128];
        snprintf_detector_name( detNamebf, 32, uID );
        snprintf( namebf,  64, "%s-profile", detNamebf );
        snprintf( labelbf, 128, "SADC profile for %s detector", detNamebf );
        res.first = new TProfile(
                namebf,
                labelbf,
                _sadcHstPars.nBins[0],
                _sadcHstPars.min[0],
                _sadcHstPars.max[0] );
        snprintf( namebf,  64, "%s-dst-profile", detNamebf );
        snprintf( labelbf, 128, "SADC distribution profile for %s detector", detNamebf );
        res.second = new TH2F(
                namebf,
                labelbf,
                _sadcHstPars.nBins[0],
                _sadcHstPars.min[0],
                _sadcHstPars.max[0],
                _sadcHstPars.nBins[1],
                _sadcHstPars.min[1],
                _sadcHstPars.max[1]
                );
    }
    return res;
}

void
SADC_WFPlotsCollector::_V_free_entry( DetectorSignature, std::pair<TProfile *, TH2F *> /*th*/ ) {
    // ... TODO
}

void
SADC_WFPlotsCollector::_V_print_brief_summary( std::ostream & ) const {
    // ... TODO
}

bool
SADC_WFPlotsCollector::_V_process_SADC_profile_event( p348::events::SADC_profile * samplesPtr ) {
    p348::events::SADC_profile & sadcProf = *samplesPtr;
    std::pair<TProfile *, TH2F *> & tPair = consider_entry( sadcProf.detectorid() );
    for( UByte i = 0; i < 32; ++i ) {
        tPair.first->Fill(i,  sadcProf.samples( i ));
        tPair.second->Fill(i, sadcProf.samples( i ));
    }
    return true;
}

p348_DEFINE_CONFIG_ARGUMENTS {
    po::options_description sadcCStats( "SADC waveform statistics collector processor (sadcCStats) options" );
    { sadcCStats.add_options()
        ("sadc-stats.store-ampls-histogram",
            po::value< p348::aux::HistogramParameters2D >(),
            "Amplitude binning boundaries for time-vs-SADC histogram.")
        ;
    }
    return sadcCStats;
}
p348_DEFINE_DATA_PROCESSOR( SADC_WFPlotsCollector ) {
    return new SADC_WFPlotsCollector(
                    goo::app<AnalysisApplication>()
                    .cfg_option<p348::aux::HistogramParameters2D>("sadc-stats.store-ampls-histogram")
            );
} p348_REGISTER_DATA_PROCESSOR( SADC_WFPlotsCollector,
    "sadcPlot",
    "Collects waveform distribution from SADC detectors."
)

}  // namespace aux
}  // namespace dprocessors
}  // namespace p348

# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)


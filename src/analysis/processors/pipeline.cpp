/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# if defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

# include <boost/asio.hpp>
# include <boost/bind.hpp>
# include <mutex>
# include <boost/circular_buffer.hpp>
# include <boost/thread.hpp>

# include "app/analysis.hpp"
# include "p348g4_uevent.hpp"
# include "p348g4_mCastSender.hpp"

namespace p348 {
namespace dprocessors {

namespace aux {
/**@brief An event-server pipeline class.
 *
 * Class implementing a pipeline approach. Represents an open queue
 * (limeted FIFO stack). It recieves a number events and keeps them
 * stacked until FIFO stack limit will be reached. Then newly added
 * events causes erasing of stack bottom.
 *
 * The EventPipelineStorage instance by itself does not provide any
 * event treatment. It only manages a circular queue of serialized
 * gprotobuf's MulticastMessage.
 * */
class EventPipelineStorage : public AnalysisApplication::iEventProcessor {
public:
    typedef ::p348::events::Event Event;
private:
    size_t _nProcessed;
    boost::circular_buffer<Event> _queue;
protected:
    std::mutex _queueMutex;

    virtual bool _push_event_to_queue( const Event & );
    virtual bool _V_process_event( Event * ) override;
public:
    EventPipelineStorage( size_t queueLength );
    ~EventPipelineStorage();

    const boost::circular_buffer<Event> & events_queue() const { return _queue; }
    boost::circular_buffer<Event> & events_queue() { return _queue; }

    size_t n_processed() const { return _nProcessed; }
    bool is_empty() const { return _queue.empty(); }
};  // class EventPipeline

// EventPipelineStorage implementation
/////////////////////////////////////

EventPipelineStorage::EventPipelineStorage( size_t queueLength ) :
            _nProcessed(0), _queue(queueLength) {
}

EventPipelineStorage::~EventPipelineStorage() {
}

bool
EventPipelineStorage::_V_process_event( Event * eventPtr ) {
    return _push_event_to_queue(*eventPtr);
}

bool
EventPipelineStorage::_push_event_to_queue( const Event & event ) {
    std::lock_guard<std::mutex> lock( _queueMutex );
    _queue.push_front();  // now back points to newly-created
    _queue.front().CopyFrom( event );
    ++_nProcessed;
    return true;
}
}  // namespace aux

/**@class EventMulticaster
 * @brief Class implementing network multicasting with deferred buffering.
 *
 * This class implements storaging and sending interfaces of event
 * multicasting API.
 */
class EventMulticaster : public net::iMulticastEventSender,
                         public aux::EventPipelineStorage {
public:
    typedef aux::EventPipelineStorage::Event Event;
    typedef ::p348::events::MulticastMessage Message;
private:
    boost::asio::io_service * _ioServicePtr;
    bool _ownIOService;
    Message _reentrantMessageKeeper;
protected:
    virtual bool _V_do_continue_transmission() const override;
    virtual void _V_send_next_message() override;
    virtual bool _V_process_event( Event * eventPtr ) override;
    virtual void _V_print_brief_summary( std::ostream & os ) const override;
public:
    EventMulticaster( const boost::asio::ip::address & multicastAddress,
                      size_t queueLength,
                      int portNo=30001,
                      boost::asio::io_service * ioServicePtr=nullptr,
                      size_t sendingBufferSize=1024*1024 );
    ~EventMulticaster();

    boost::asio::io_service & ioservice() { return *_ioServicePtr; }
    const boost::asio::io_service & ioservice() const { return *_ioServicePtr; }
};  // class EventMulticaster


// EventMulticaster
//////////////////

EventMulticaster::EventMulticaster( const boost::asio::ip::address & multicastAddress,
                                    size_t queueLength,
                                    int portNo,
                                    boost::asio::io_service * ioServicePtr,
                                    size_t sendingBufferSize ) :
            net::iMulticastEventSender( *(ioServicePtr ? ioServicePtr : new boost::asio::io_service()),
                               multicastAddress, portNo, sendingBufferSize ),
            aux::EventPipelineStorage( queueLength ),
            _ioServicePtr( &(net::iMulticastEventSender::socket().get_io_service()) ),
            _ownIOService(!ioServicePtr) {
}

EventMulticaster::~EventMulticaster() {
    if( _ioServicePtr && _ownIOService ) {
        delete _ioServicePtr;
    }
}

bool
EventMulticaster::_V_do_continue_transmission() const {
    return !events_queue().empty();
}

void
EventMulticaster::_V_send_next_message() {
    // Note: this method can be invoked either by event-treatment method,
    // either by sending thread handling end-of transmission for non-empty
    // queue.
    std::lock_guard<std::mutex> lock(_queueMutex);
    // Sometimes evaluation meets the empty queue here because
    // of sending thread have sent last event in queue and treatment
    // thread comes here meeting empty queue.
    if( !is_empty() ) {
        _reentrantMessageKeeper.Clear();
        _reentrantMessageKeeper.mutable_event()->CopyFrom( events_queue().front() );
        send_message( _reentrantMessageKeeper );
        events_queue().pop_front();
    }
}

bool
EventMulticaster::_V_process_event( Event * eventPtr ) {
    bool insertionResult = aux::EventPipelineStorage::_V_process_event( eventPtr );
    sending_mutex().lock();
    if( !net::iMulticastEventSender::is_operating() ) {
        _V_send_next_message();
    }
    sending_mutex().unlock();
    return insertionResult;
}

void
EventMulticaster::_V_print_brief_summary( std::ostream & os ) const {
    os << ESC_CLRGREEN "Event multicasting processor" ESC_CLRCLEAR ":" << std::endl
       << "  number of events processed . : " << n_processed() << std::endl;
    // TODO: ... other stuff
}

// Register processor:
p348_DEFINE_CONFIG_ARGUMENTS {
    po::options_description multicastP( "Multicasting (network multicast)" );
    { multicastP.add_options()
        ("multicast.address",
            po::value<std::string>()->default_value("239.255.0.1"),
            "Multicast address to use." )
        ("multicast.port",
            po::value<int>()->default_value(30001),
            "Multicast port number.")
        ("multicast.storage-capacity",
            po::value<size_t>()->default_value(500),
            "Event to be stored. Defines the capacitance of last read events.")
        ;
    }
    return multicastP;
}
p348_DEFINE_DATA_PROCESSOR( EventMulticaster ) {
    auto p = new EventMulticaster(
            boost::asio::ip::address::from_string(
                goo::app<p348::AbstractApplication>().cfg_option<std::string>("multicast.address")
            ),
            goo::app<p348::AbstractApplication>().cfg_option<size_t>("multicast.storage-capacity"),
            goo::app<p348::AbstractApplication>().cfg_option<int>("multicast.port"),
            goo::app<p348::AbstractApplication>().boost_io_service_ptr()
        );
    //io_service.run();
    return p;
} p348_REGISTER_DATA_PROCESSOR(
    EventMulticaster,
    "multicast",
    "An asynchroneous event-multicasting pipeline." )

}  // namespace p348
}  // namespace dprocessors

# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)


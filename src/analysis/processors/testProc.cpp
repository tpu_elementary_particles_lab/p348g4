/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# if defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

# include "app/analysis.hpp"
# include "p348g4_uevent.hpp"

namespace p348 {
namespace dprocessors {
namespace aux {

class TestingProcessor : public AnalysisApplication::iEventProcessor {
public:
    typedef AnalysisApplication::iEventProcessor Parent;
    typedef AnalysisApplication::Event Event;
protected:
    virtual bool _V_process_event( Event * ) override;
};  // class TestingProcessor


// IMPLEMENTATION

bool
TestingProcessor::_V_process_event( Event * uEvent ){
    # if 1
    if(    uEvent->has_experimental()
        && uEvent->experimental().is_physical()
        && uEvent->experimental().sadc_data_size() > 0 ) {
        for( int nSADCData = 0;
             nSADCData < uEvent->experimental().sadc_data_size();
             ++nSADCData ) {
            //UniqueDetectorID uID {.wholenum = (UShort) uEvent->experimental().sadc_data( nSADCData ).detectorid()};
            UniqueDetectorID uID( uEvent->experimental().sadc_data( nSADCData ).detectorid() );
            if( uEvent->experimental().sadc_data( nSADCData ).samples_size() ) {
                std::cout
                    << (int) nSADCData << "/" << uEvent->experimental().sadc_data_size()
                    << detector_name_by_code((EnumScope::MajorDetectorsCode) uID.byNumber.major )
                          << " : ";
                for( int nSample = 0;
                     nSample < uEvent->experimental().sadc_data( nSADCData ).samples_size();
                     ++nSample ) {
                    std::cout
                        << uEvent->experimental().sadc_data( nSADCData ).samples( nSample )
                        << " ";

                }
                std::cout << "." << std::endl;
                //std::cout << uEvent->experimental().sadc_data( nSADCData ).samples_size() << std::endl;  // XXX
            }
        }
        std::cout << " === " << std::endl;
        return true;
    }
    # endif
    return false;
}

p348_DEFINE_DATA_PROCESSOR( TestingProcessor ) {
    return new TestingProcessor();
} p348_REGISTER_DATA_PROCESSOR( TestingProcessor,
    "testing",
    "A testing processor --- filters events by common parameters. "
    "Can be configured by options prefixed by \"--evTest.*\"." )

}  // namespace aux
}  // namespace dprocessors
}  // namespace p348

# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)


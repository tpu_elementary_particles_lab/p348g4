/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# if defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)

# include "app/analysis.hpp"
# include "p348g4_uevent.hpp"

namespace p348 {
namespace dprocessors {
namespace evd {

class HCALPreproc : public AnalysisApplication::iSADCProcessor {
public:
    typedef AnalysisApplication::iEventProcessor Parent;
    typedef AnalysisApplication::Event Event;
private:
    /// HCALs summary groups cache.
    std::unordered_map<DetectorMajor, events::DetectorSummary *> _cache_evdMG;
protected:
    /// Besides of treatment of experimental events this routes call of modelled ones.
    virtual bool _V_process_event( Event * ) override;
    /// Calculates sum integral in HCAL to provide.
    bool _V_process_experimental_event( p348::events::ExperimentalEvent * ) override;
    /// Calculates waveform integral in group.
    virtual bool _V_process_SADC_profile_event( p348::events::SADC_profile * ) override;
    /// Resets detector subgroup cache after event is done (and sent).
    virtual void _V_finalize_event_processing( Event * ) override;
    /// Prints summary of processor after everything done.
    virtual void _V_print_brief_summary( std::ostream & ) const override;
    /// Cleanup.
    virtual void _V_finalize() const override;
};  // class HCALPreproc

bool
HCALPreproc::_V_process_event( Event * event ) {
    if( event->has_simulated() ) {
        _TODO_  // TODO: process simulated event
    } else {
        return AnalysisApplication::iSADCProcessor::_V_process_event( event );
    }
}

bool
HCALPreproc::_V_process_experimental_event( p348::events::ExperimentalEvent * expEve ) {
    bool res = AnalysisApplication::iSADCProcessor::_V_process_experimental_event( expEve );
    //_cache_evdMG.mutable_maxdep()->set_amplitude( _cache_overallEDep );
    return res;
}

bool
HCALPreproc::_V_process_SADC_profile_event( p348::events::SADC_profile * sadcProfMutablePtr ) {
    p348::events::SADC_profile & sadcProf = *sadcProfMutablePtr;
    // Ignore non-HCAL SADCs:
    UniqueDetectorID detID( sadcProf.detectorid() );
    if( detector_family_num( detID.byNumber.major ) != EnumScope::fam_HCAL ) {
        return true;
    }

    assert( sadcProf.samples_size() == 32 );
    float ampSum = 0.;

    // if sum is already calculated, use it; otherwise calculate it here
    if(!( sadcProf.has_suppinfo() && (ampSum = sadcProf.suppinfo().sum()) )) {
        for( int nSample = 0; nSample < sadcProf.samples_size(); ++nSample ) {
            ampSum += sadcProf.samples( nSample );
        }
    }

    auto cacheHCALIt = _cache_evdMG.find( detID.byNumber.major );
    if( _cache_evdMG.end() == cacheHCALIt ) {
        cacheHCALIt = _cache_evdMG.emplace(
                    detID.byNumber.major,
                    mixins::PBEventApp::c_event()
                    .mutable_eventdisplaymessage()
                        ->add_summary()
                ).first;
        cacheHCALIt->second->set_detectorid( detID.byNumber.major );
    }
    events::DetectorSummary & sm = *(cacheHCALIt->second);
    events::DetectorSummary * thisSummary = sm.mutable_subgroup()->add_summary();
    thisSummary->set_detectorid( detID.wholenum );
    thisSummary->mutable_edep()->set_amplitude( ampSum );

    if( sm.subgroup().maxdep().amplitude() < ampSum ) {
        sm.mutable_subgroup()->mutable_maxdep()->set_amplitude( ampSum );
    }

    return true;
}

void
HCALPreproc::_V_finalize_event_processing( Event * ) {
    // Clear HCAL subgroup cache ptr for this event.
    _cache_evdMG.clear();
}

void
HCALPreproc::_V_print_brief_summary( std::ostream & ) const {
}

void
HCALPreproc::_V_finalize() const {
}

p348_DEFINE_DATA_PROCESSOR( HCALPreproc ) {
    return new HCALPreproc();
} p348_REGISTER_DATA_PROCESSOR(
    HCALPreproc,
    "evd-HCAL",
    "Event display preprocessor for hadronic calorimeter (HCAL)." )

}  // namespace evd
}  // namespace dprocessors
}  // namespace p348

# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES)



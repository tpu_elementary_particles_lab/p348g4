/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "app/analysis.hpp"

# ifdef RPC_PROTOCOLS

# include "event.pb.h"
# include "app/mixins/root.hpp"

# include <TFile.h>

/**@defgroup analysis Analysis
 *
 * @brief This group provides various analysis routines for processing
 * experimental or modelled data.
 * */

namespace p348 {

// Dictionaries stuff
// - Readers

std::unordered_map<std::string, std::pair<AnalysisApplication::EvSeqCtr, std::string> >  *
    AnalysisApplication::_readersDict = nullptr;

void
AnalysisApplication::add_reader( const std::string & key, EvSeqCtr ctr, const std::string & descr ) {
    if( !_readersDict ) {
        _readersDict = new std::remove_pointer<DECLTYPE(_readersDict)>::type;
    }
    auto insertionResult = _readersDict->emplace( key,
            std::pair<AnalysisApplication::EvSeqCtr, std::string>( ctr, descr ) );
    if( !insertionResult.second ) {
        std::cerr << "Failed to register an event sequence reader \""
                  << key << "\" because of "
                  << "this name isn't unique, or this reader format already known."
                  << std::endl
                  ;
    }
}

void
AnalysisApplication::list_readers( std::ostream & os ) {
    if( !_readersDict || _readersDict->empty() ) {
        os << "No formats available." << std::endl;
    } else {
        os << "Available data formats:" << std::endl;
        for( auto it  = _readersDict->begin();
                  it != _readersDict->end(); ++it) {
            os << "  - " << it->first << std::endl
               << "  " << (void*) it->second.first
               << " : " << it->second.second << std::endl;
        }
    }
}

AnalysisApplication::EvSeqCtr
AnalysisApplication::find_reader( const std::string & key ) {
    if( !_readersDict ) {
        emraise( badState, "No readers registered." )
    }
    auto it = _readersDict->find(key);
    if( _readersDict->end() == it ) {
        emraise( notFound, "Format unknown: \"%s\".", key.c_str() );
    }
    return it->second.first;
}

// - Processors

std::unordered_map<std::string, std::pair<AnalysisApplication::EvProcCtr, std::string> > *
    AnalysisApplication::_procsDict = nullptr;

void
AnalysisApplication::add_processor( const std::string & key, EvProcCtr ctr, const std::string & descr ) {
    if( !_procsDict ) {
        _procsDict = new std::remove_pointer<DECLTYPE(_procsDict)>::type;
    }
    auto insertionResult = _procsDict->emplace( key,
            std::pair<AnalysisApplication::EvProcCtr, std::string>( ctr, descr ) );
    if( !insertionResult.second ) {
        std::cerr << "Failed to register an event processor \""
                  << key << "\" because of "
                  << "this name isn't unique, or this processor already known."
                  << std::endl
                  ;
    }
}

void
AnalysisApplication::list_processors( std::ostream & os ) {
    if( !_procsDict || _procsDict->empty() ) {
        os << "No processors available." << std::endl;
    } else {
        os << "Available data processors:" << std::endl;
        for( auto it  = _procsDict->begin();
                  it != _procsDict->end(); ++it) {
            os << "  - " << it->first << std::endl
               << "  " << (void*) it->second.first
               << " : " << it->second.second << std::endl;
        }
    }
}

AnalysisApplication::EvProcCtr
AnalysisApplication::find_processor( const std::string & key ) {
    if( !_procsDict ) {
        emraise( badState, "No processors registered." )
    }
    auto it = _procsDict->find(key);
    if( _procsDict->end() == it ) {
        emraise( notFound, "Processor unknown: \"%s\".", key.c_str() );
    }
    return it->second.first;
}

void
AnalysisApplication::push_back_processor( iEventProcessor * proc ) {
    if( !proc ) {
        emraise( nullPtr, "Can't add a processor --- null pointer." );
    }
    _processorsChain.push_back( proc );
    p348g4_log3( "Processor %p now handles event pipeline.\n", proc );
}

void
AnalysisApplication::push_back_processor( const std::string & name ) {
    auto p = find_processor( name )();
    p348g4_log3( "Adding processor %s:%p.\n", name.c_str(), p );
    push_back_processor( p );
}


// Processors subtypes

bool
AnalysisApplication::iExperimantalEventProcessor::_V_process_event( Event * uEvent ) {
    if(    uEvent->has_experimental()
        && uEvent->experimental().is_physical() ) {
        return _V_process_experimental_event( uEvent->mutable_experimental() );
    }
    return false;
}

bool
AnalysisApplication::iSADCProcessor::_V_process_experimental_event( p348::events::ExperimentalEvent * expEvent ) {
    if( ! expEvent->sadc_data_size() ) {
        return false;
    }
    bool result = true;
    for( int nSADCData = 0;
             nSADCData < expEvent->sadc_data_size();
             ++nSADCData ) {
        result &= _V_process_SADC_profile_event( expEvent->mutable_sadc_data(nSADCData) );
    }
    result &= _V_finalize_processing_sadc_event( expEvent );
    return result;
}

bool
AnalysisApplication::iAPVProcessor::_V_process_experimental_event( p348::events::ExperimentalEvent * expEvent ) {
    if( ! expEvent->apv_data_size() ) {
        return false;
    }
    bool result = true;
    for( int nAPVData = 0;
             nAPVData < expEvent->apv_data_size();
             ++nAPVData ) {
        result &= _V_process_APV_samples( expEvent->mutable_apv_data(nAPVData) );
    }
    result &= _V_finalize_processing_apv_event( expEvent );
    return result;
}

// Options supplementing routines.

std::unordered_set<AnalysisApplication::OptionsSupplement> *
AnalysisApplication::_suppOpts = nullptr;

void
AnalysisApplication::supp_options( OptionsSupplement optsSupp ) {
    if( !_suppOpts ) {
        _suppOpts = new std::unordered_set<OptionsSupplement>();
    }
    _suppOpts->insert( optsSupp );
}

// Application methods

AnalysisApplication::AnalysisApplication( Config * vm ) :
            p348::AbstractApplication( vm ),
            mixins::PBEventApp(vm),
            _evSeq(nullptr) {
    _enable_ROOT_feature( mixins::RootApplication::enableCommonFile );
    // Inject custom stacktrace info acauizition into boost exception construction
    // procedures; TODO: doesn't work; may be here `handler' means "how to treat" the
    // exception, not "hook for user code to throw one"?)
    //init_boost_exception_handler();

    // XXX (dev)
    {
        namespace ga = ::goo::aux;
        # ifdef GOO_GDB_EXEC
        ga::iApp::add_handler(
                ga::iApp::_SIGABRT,
                ga::iApp::attach_gdb,
                "Attaches gdb to a process on on SIGABRT."
            );
        # endif
        # ifdef GOO_GCORE_EXEC
        ga::iApp::add_handler(
                ga::iApp::_SIGABRT,
                ga::iApp::dump_core,
                "Creates a core.<pid> coredump file on SIGABRT.",
                false
            );
        # endif
    }
}

AnalysisApplication::~AnalysisApplication() {
    if( _evSeq ) {
        delete _evSeq;
        _evSeq = nullptr;
    }
    for( auto it  = _processorsChain.begin();
              it != _processorsChain.end(); ++it ) {
        delete *it;
    }
    _processorsChain.clear();
    if(gFile) {
        gFile->Write();
    }
}

std::vector<po::options_description>
AnalysisApplication::_V_get_options() const {
    std::vector<po::options_description> res = Parent::_V_get_options();
    po::options_description analysisAppCfg;
    { analysisAppCfg.add_options()
        ("input-file,i",
            po::value<std::string>(),
            "Input file --- an actual data source.")
        ("input-format,F",
            po::value<std::string>()->default_value("unset"),
            "Sets input file format.")
        ("processor,p",
            po::value<std::vector<std::string>>(),
            "Pushes processor in chain, one by one, in order.")
        ("list-src-formats",
            "Prints a list of available input file data formats.")
        ("list-processors",
            "Prints a list of available treatment routines.")
        ("max-events-to-read,n",
            po::value<size_t>()->default_value(0),
            "Number of events to read; (set zero to read all available).")
        //("root-file,R",
        //    po::value<std::string>()->default_value("/tmp/p348a_last.root"),
        //    "ROOT file where results will be written.")
        ;
    } res.push_back(analysisAppCfg);
    if( _suppOpts ) {
        for( auto it = _suppOpts->begin(); it != _suppOpts->end(); ++it ) {
            res.push_back( (*it)() );
        }
    }
    return res;
}

void
AnalysisApplication::_V_configure_concrete_app() {
    po::variables_map & vm = co();
    if( vm.count("list-src-formats") ) {
        list_readers( std::cout );
        _immediateExit = true;
    }
    if( vm.count("list-processors") ) {
        list_processors( std::cout );
        _immediateExit = true;
    }
    if( !do_immediate_exit() && _readersDict && cfg_option<std::string>("input-format") != "unset" ) {
        _evSeq = find_reader( cfg_option<std::string>("input-format") )();
    }
    if( !do_immediate_exit() && _procsDict ) {
        auto procNamesVect = cfg_option<std::vector<std::string>>("processor");
        for( auto it  = procNamesVect.begin();
                  it != procNamesVect.end(); ++it) {
            push_back_processor( *it );
        }
    }
    //if( !cfg_option<std::string>("root-file").empty() ) {
    //    new TFile( cfg_option<std::string>("root-file").c_str(), "RECREATE" );
    //}
}

}  // namespace p348

# endif  // RPC_PROTOCOLS


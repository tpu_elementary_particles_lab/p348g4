/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL

# include "g4extras/auxInfoProcessor.hpp"
# include "app/app.h"

# include <Geant4/G4LogicalVolumeStore.hh>
# include <Geant4/G4GDMLParser.hh>

namespace p348 {

GDMLAuxInfoProcessor * GDMLAuxInfoProcessor::_self = nullptr;

GDMLAuxInfoProcessor::GDMLAuxInfoProcessor() :
    _logStoreCachePtr(G4LogicalVolumeStore::GetInstance()) {
}

GDMLAuxInfoProcessor::~GDMLAuxInfoProcessor() {
}

GDMLAuxInfoProcessor &
GDMLAuxInfoProcessor::self() {
    return *(_self ? _self : (_self = new GDMLAuxInfoProcessor()));
}

const GDMLAuxInfoProcessor &
GDMLAuxInfoProcessor::const_self() {
    return *(_self ? _self : (_self = new GDMLAuxInfoProcessor()));
}

void
GDMLAuxInfoProcessor::add_processing_routine( const std::string & name,
                                              PropertyTreatmentCallback cllb ) {
    _callbacks[name] = cllb;
}

void
GDMLAuxInfoProcessor::apply( G4GDMLParser & parser ) {
    // iterate among all known logical volumes
    for( auto lvciter  = _logStoreCachePtr->begin();
              lvciter != _logStoreCachePtr->end();
              lvciter++ ) {
        G4GDMLAuxListType auxInfo = parser.GetVolumeAuxiliaryInformation(*lvciter);
        G4LogicalVolume * const tVol = *lvciter;
        for( auto ipair = auxInfo.begin(); ipair != auxInfo.end(); ipair++ ) {
                const G4String & aiType  = ipair->type,
                                 aiValue = ipair->value;
            auto dictPairIt = _callbacks.find( aiType );
            if( _callbacks.end() == dictPairIt ) {
                p348g4_logw( "Found unknown auxinfo property type `%s' with value `%s' at volume `%s'. Skipping.\n",
                          ipair->type.c_str(),
                          ipair->value.c_str(),
                          tVol->GetName().c_str());
                continue;
            } else {
                dictPairIt->second( tVol, ipair->value );
            }
        }
    }
}

}  // namespace p348

# endif  // GEANT4_MC_MODEL


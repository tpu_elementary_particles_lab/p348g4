/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL

# include <G4ParticleGun.hh>
# include <G4ParticleTable.hh>

# include "g4extras/PGA.hpp"
# include "g4extras/PGA/g4_simpleGunPGA.hh"
# include "p348g4_utils.hpp"

# ifdef G4_APRIME_G4_PROCESS
    # include "g4extras/physics/AParticle.hpp"
# endif
# include "app/abstract.hpp"

namespace p348g4 {

PrimaryGeneratorActionPG::PrimaryGeneratorActionPG(
            const std::string & particleType,
            const G4double & energyMeV,
            const G4ThreeVector & position,
            const G4ThreeVector & direction
    ) :
        G4VUserPrimaryGeneratorAction(),
        _pGunPtr(nullptr) {
    _pGunPtr = new G4ParticleGun( 1 );

    G4ParticleTable * particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition * particle
        = particleTable->FindParticle( particleType );
    // TODO for A'
    # ifdef G4_APRIME_G4_PROCESS 
    if ( particleType == "APrime" ) {
        particle = APrime::APrimeDefinition();    
    }
    # endif
    if( !particle ) {
        emraise( malformedArguments, "Couldn't find particle \"%s\".", particleType.c_str() );
    }

    _pGunPtr->SetParticleDefinition( particle );
    _pGunPtr->SetParticleEnergy( energyMeV*CLHEP::MeV );
    _pGunPtr->SetParticlePosition( position );
    _pGunPtr->SetParticleMomentumDirection( direction );

    p348g4_log1( "Simple particle gun %p created at {%.2e, %.2e, %.2e} / {%.2e, %.2e, %.2e} for %s particles of %e MeV.\n",
              _pGunPtr,
              position.x(),     position.y(),   position.z(),
              direction.x(),    direction.y(),  direction.z(),
              particle->GetParticleName().c_str(),
              energyMeV );
}

void
PrimaryGeneratorActionPG::GeneratePrimaries( G4Event * anEvent ) {
    _pGunPtr->GeneratePrimaryVertex( anEvent );
}

PrimaryGeneratorActionPG::~PrimaryGeneratorActionPG() {
    if( _pGunPtr ) {
        delete _pGunPtr;
    }
}

//
// Constructor
//

static G4VUserPrimaryGeneratorAction *
new_simple_gun_PGA( G4UImessenger *& messenger ) {
    const p348::po::variables_map & vm = goo::app<p348::AbstractApplication>().co();
    // Obtain default parameters...
    auto pga = new PrimaryGeneratorActionPG(
        vm["g4-simpleGun.particleType"].as<std::string>(),
        vm["g4-simpleGun.energy-MeV"].as<double>(),
        p348::aux::parse_g4_three_vector(
                    vm["g4-simpleGun.position-cm"].as<std::string>(),
                    CLHEP::cm
                ),
        p348::aux::parse_g4_three_vector(
                    vm["g4-simpleGun.direction"].as<std::string>()
                )
        );
    // Geant4 particle gun has its own nice messenger.
    messenger = nullptr;
    return pga;
}

P348_PGA_CONSTRUCTOR( SimpleGun, new_simple_gun_PGA )

}  // namespace p348g4

# endif  // GEANT4_MC_MODEL


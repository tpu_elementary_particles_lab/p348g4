/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# if 1

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL
# ifdef G4_APRIME_G4_PROCESS 

# include "app/app.h"

# include "g4extras/physics/AParticle.hpp"

# include <G4ParticleTable.hh>
# include <G4SystemOfUnits.hh>
# include <G4PhaseSpaceDecayChannel.hh>
# include <G4DalitzDecayChannel.hh>
# include <G4DecayTable.hh>

namespace p348g4 {

APrime * APrime::theInstance = nullptr;

APrime::APrime(){
    p348g4_log1("<<< A' ctr invokation here!\n");  // XXX???
}

APrime::~APrime(){
}

APrime *
APrime::Definition() {
    if( theInstance ) {
        return theInstance;
    }
    const G4String name = "APrime";
    // search in particle table]
    G4ParticleTable * pTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition * anInstance = pTable->FindParticle(name);
    if( !anInstance ) {
        // Create the particle:
        //    Arguments for constructor are as follows
        //               name             mass          width         charge
        //             2*spin           parity  C-conjugation
        //          2*Isospin       2*Isospin3       G-parity
        //               type    lepton number  baryon number   PDG encoding
        //             stable         lifetime    decay table
        //             shortlived      subType    anti_encoding
        // use constants in CLHEP
        //  static const double electron_mass_c2 = 0.51099906 * MeV;
        // TODO:
        // TODO G4double APrimeMass_GeV = goo::app<p348::AbstractApplication>().cfg_option<double>("aprimeEvGen.massA_GeV"); 
        G4double APrimeMass_GeV = 0.01;
        // TODO G4double mixingFactor = goo::app<p348::AbstractApplication>().cfg_option<double>("aprimeEvGen.mixingFactor");
        G4double mixingFactor = 1e-4;
        G4double lifeTime_s = 80.0/(3*pow(10., 8.)) * 
                pow(10., -6.) *
                pow( pow(10.,-4.)/mixingFactor, 2. ) * 
                (100*pow(10.,6.)/( APrimeMass_GeV*pow(10.,9) ) );
        //std::cout << "lifeTime_s: " << lifeTime_s << std::endl;  // XXX
        G4double decayWidth_eV = 6.582*pow(10., -16.) / lifeTime_s;
        //std::cout << "decayWidth_eV: " << decayWidth_eV << std::endl;  // XXX
        anInstance = new G4ParticleDefinition(
                /* Name ..................... */ name,
                /* Mass ..................... */ APrimeMass_GeV*GeV,  // TODO
                /* Decay width .............. */ decayWidth_eV*eV,    // TODO
                /* Charge ................... */ 0.*eplus,
		        /* 2*spin ................... */ 0,
                /* parity ................... */ 0,
                /* C-conjugation ............ */ 0,
		        /* 2*Isospin ................ */ 0,
                /* 2*Isospin3 ............... */ 0,
                /* G-parity ................. */ 0,
	            /* type ..................... */ "boson",
                /* lepton number ............ */ 0,
                /* baryon number ............ */ 0,
                /* PDG encoding ............. */ 90,    // TODO for generator specific needs reserved numbers 81-100 
		        /* stable ................... */ false,  // TODO: false
                /* lifetime.................. */ lifeTime_s*s,  // TODO
                /* decay table .............. */ NULL,  // TODO
                /* shortlived ............... */ false,  // TODO
                /* subType .................. */ "geantino",
                /* anti particle encoding ... */ 90    // TODO: ???
            );

        // Create decay table
        G4DecayTable* decayTable = new G4DecayTable();

        // Create decay channel
        G4VDecayChannel* mode;

        // Create mode A'->e-e+
        mode = new G4PhaseSpaceDecayChannel("APrime", 1.0, 2, "e-", "e+");
        
        // Insert mode to the table
        decayTable->Insert(mode);
        
        // Set decay table to A'
        anInstance->SetDecayTable(decayTable);

        // Bohr Magnetron
        G4double muB =  0 ;
        anInstance->SetPDGMagneticMoment( muB * 2.* 1.0011596521859 );


    }
    theInstance = reinterpret_cast<APrime*>(anInstance);
    return theInstance;
}

APrime *
APrime::APrimeDefinition() {
    return Definition();
}

//APrime *
//APrime::APrime() {
//    return Definition();
//}

}  // namespace p348g4

# endif  // G4_APRIME_G4_PROCESS
# endif  // GEANT4_MC_MODEL
# endif

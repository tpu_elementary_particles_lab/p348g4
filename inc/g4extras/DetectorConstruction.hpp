/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348_G4_SIM_COMMON_DETECTOR_CONSTRUCTION_H
# define H_P348_G4_SIM_COMMON_DETECTOR_CONSTRUCTION_H

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL

# include <G4VUserDetectorConstruction.hh>
# include <G4VPhysicalVolume.hh>

namespace p348 {
namespace g4sim {

/// Detector construction allowing to use the geometry read from the GDML file.
class DetectorConstruction :
      public G4VUserDetectorConstruction {
private:
    G4VPhysicalVolume * _worldPtr;
public:
    DetectorConstruction(G4VPhysicalVolume *setWorld = 0);
    virtual G4VPhysicalVolume * Construct() override;
};

}  // namespace g4sim
}  // namespace p348

# endif  // GEANT4_MC_MODEL

# endif  // H_P348_G4_SIM_COMMON_DETECTOR_CONSTRUCTION_H


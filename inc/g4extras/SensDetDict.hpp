/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348_G4_SENSITIVE_DETECTOR_DICTIONARY_H
# define H_P348_G4_SENSITIVE_DETECTOR_DICTIONARY_H

# include "app/mixins/geant4.hpp"

# ifdef GEANT4_MC_MODEL

# include <unordered_map>

class G4VSensitiveDetector;

namespace p348 {

class SDDictionary {
public:
    typedef G4VSensitiveDetector * (*SDCtr)( const std::string & );
private:
    static SDDictionary * _self;
    /// Dictionary indexing SD's constructors.
    std::unordered_map<std::string, SDCtr> _dict;
    /// TODO: ignore detectors list/regex
    SDDictionary() {}
    virtual ~SDDictionary();
public:
    static SDDictionary & self();
    void register_SD_inststance(
            const std::string &,
            SDCtr );

    void print_SD_List();
    
    SDCtr operator[]( const std::string & ) const;
};

}  // namespace p348

# define P348_G4_REGISTER_SD( name ) \
static G4VSensitiveDetector * __SD_ctr_ ## name ( const std::string & instName ) { \
    return new name( instName ); } \
static void __ctr_register_ ## name () __attribute__(( __constructor__(156) )); \
static void __ctr_register_ ## name () { \
    p348::SDDictionary::self().register_SD_inststance( # name, __SD_ctr_ ## name ); }

# endif  // GEANT4_MC_MODEL
# endif  // H_P348_G4_SENSITIVE_DETECTOR_DICTIONARY_H


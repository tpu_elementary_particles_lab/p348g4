/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348_G4_FIELD_DICTIONARY_H
# define H_P348_G4_FIELD_DICTIONARY_H

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL

# include <G4ThreeVector.hh>
# include <unordered_map>

class G4Field;

namespace p348 {

class FieldDictionary {
public:
    typedef G4Field * (*FieldCtr)( const G4ThreeVector & );
private:
    static FieldDictionary * _self;
    /// Dictionary indexing Field's constructors.
    std::unordered_map<std::string, FieldCtr> _dict;
    /// TODO: ignore detectors list/regex
    FieldDictionary() {}
    virtual ~FieldDictionary();
public:
    static FieldDictionary & self();
    void register_field_instance(
            const std::string &,
            FieldCtr );

    void print_field_list();
    
    FieldCtr operator[]( const std::string & ) const;
};

}  // namespace p348

# define P348_G4_REGISTER_FIELD( name ) \
static G4Field * __Field_ctr_ ## name ( const G4ThreeVector & fieldValue) { \
    return new name( fieldValue ); } \
static void __ctr_register_ ## name () __attribute__(( __constructor__(156) )); \
static void __ctr_register_ ## name () { \
    p348::FieldDictionary::self().register_field_instance( # name, __Field_ctr_ ## name ); }

# endif  // GEANT4_MC_MODEL
# endif  // H_P348_G4_FIELD_DICTIONARY_H



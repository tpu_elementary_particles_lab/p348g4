/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_AMIXINPROCESS_H
# define H_AMIXINPROCESS_H

/**@brief A'-production process implementation header.
 * @file AMixinProcess.hpp
 *
 * Provides declaration of process (in terms of Geant4) implementing
 * mixin A' photoproduction.
 *
 * Here, in order to describe A' physics we shall:
 *  - Define mixin photoproduction approximation of an every charged
 *    particle (descendant of PostStep process class);
 *  - Define AtRest class descendant process describin A' decay.
 *
 * In general, there are at least two convinient ways to embed custom
 * A' mixing physics in Geant4:
 *  1. Inheriting from a G4HadronicProcess class;
 *  2. Using a direct inheritance from G4VDiscreteProcess.
 *
 * The first way is the way all the standard Geant4 hadronic processes
 * are implemented. It relies on numerous «models» and cross section
 * data sets and so on. The second way is rather strightforward and doen't
 * require immersing into Geant4 guts, however, demands us to provide
 * event-generation algorithm (based on TFoam or whatever) besides of
 * cross-sections. Which approach is to be used
 * is defined by AMIXING_PROCESS_USES_HADRONIC_API macros.
 */

# include "p348g4_config.h"

# ifdef G4_APRIME_G4_PROCESS

# include <Geant4/G4HadronicProcess.hh>
# include "g4extras/physics/AParticle.hpp"

class TRandom;  // fwd
namespace p348 {
class APrimeGenerator;  // fwd
};

namespace p348g4 {

class APrimePhysicsCache;

# ifndef AMIXING_PROCESS_USES_HADRONIC_API

/**@class AMixingProcess
 * @brief A class implementing A' production process in terms of Geant4 API.
 *
 * Inherited from G4VDiscreteProcess, this class implies a sequential
 * invokations of PostStepDoIt() AFTER the GetMeanFreePath() as it caches
 * some information between invokations.
 * */
class AMixingProcess : public G4VDiscreteProcess {
public:
    /// Common definition of A' particle pointer.
    APrime * theAPrimePtr;
private:
    uint8_t _minAWW;
    double _minE;
protected:
    /// Cache that stores last generators and probability factor $\sigma \cdot n$.
    /// Pairs are ordered ascending by the first value.
    mutable std::vector<std::pair<double, p348::APrimeGenerator *> > _mruGeneratorsCache;
    /// Last used mean free path.
    mutable double _mruMFP;
    const double _AMass,           ///< A' mass, initialized from ctr.
                 _mixingConstant;  ///< WW mixing constant, initialized from ctr.
public:
    AMixingProcess() = delete;
    AMixingProcess( const AMixingProcess & ) = delete;

    /// Only allowed ctr for process class.
    AMixingProcess( TRandom *,
                    double AMass,
                    double mixingConstant );

    /// Frees caches only.
    ~AMixingProcess();

    /// Implements final state parameters when process won.
    virtual G4VParticleChange* PostStepDoIt( const G4Track & ,
			                                 const G4Step & ) override;

    /// Sets minimal element atomic number to be considered.
    void minimal_material_A( uint8_t nA ) { _minAWW = nA; }

    /// Set minimal projectile energy to be considered.
    void minimal_projectile_energy( double minE ) { _minE = minE; }

    /// Sets minimal element atomic number to be considered.
    uint8_t minimal_material_A() const { return _minAWW; }

    /// Set minimal projectile energy to be considered.
    double minimal_projectile_energy() const { return _minE; }

    /// Returns event probability, mm^2 (used internally by Geant4 API).
    virtual G4double GetMeanFreePath( const G4Track & aTrack,
                                      G4double previousStepSize,
                                      G4ForceCondition * condition );

    /// A' particle mass getter (set by ctr).
    double aprime_mass() const { return _AMass; }

    /// A' WW-mixing constant getter (set by ctr).
    double mixing_constant() const { return _mixingConstant; }

    /// Returns true for charged particles.
    virtual G4bool IsApplicable(const G4ParticleDefinition &) override;
};  // class AMixingProcess

# else  // AMIXING_PROCESS_USES_HADRONIC_API

class AMixingProcess : public G4HadronicProcess {
public:
    /// Default ctr
    AMixingProcess();

    /// Copy ctr
    AMixingProcess( const AMixingProcess & );
    ~AMixingProcess();

    /// Generates secondaries.
    virtual G4VParticleChange * PostStepDoIt( const G4Track& track,
                                              const G4Step& step) override;

    /// Computes integral probability along path in matter.
    virtual G4double GetMeanFreePath( const G4Track &    aTrack,
                                      G4double           previousStepSize,
                                      G4ForceCondition * condition ) override;
protected:
};  // class AMixingProcess

# endif  // AMIXING_PROCESS_USES_HADRONIC_API

}  // namespace p348g4

# endif  // G4_APRIME_G4_PROCESS

# endif  // H_AMIXINPROCESS_H


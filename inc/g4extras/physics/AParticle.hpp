/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_APRIME_PARTICLE_DEFINITION_H
# define H_APRIME_PARTICLE_DEFINITION_H

# include "p348g4_config.h"

# ifdef GEANT4_MC_MODEL

# include <G4ParticleDefinition.hh>

namespace p348g4 {

/**@brief Represents an A'-particle (sometames called «dark photon»).
 * @class APrime
 *
 * Definition of a custom particle in Geant4 is required to represent
 * all connected process. A' does not participate in any interactions
 * except gravitational and reveals itself by absence of energy in
 * electromagnetic showers when producted or by decay in quite
 * sophisticated schemas involving postdproduction of other exotic
 * particles (this decay cascades are in our TODO-list).
 *
 * All the particles in Geant4 are implemented as singletons.
 *
 * TODO: in Definition() there are a lot of wrong information about
 * this particle that must be corrected further (isospin numbers,
 * mass, etc).
 */
class APrime : public G4ParticleDefinition {
private:
    static APrime * theInstance;
    APrime();
    ~APrime();
public:
    /// Geant4's std way of particle definition declaration.
    static APrime * Definition();
    /// Geant4's std alias for Definition().
    static APrime * APrimeDefinition();
    // Note: another alias for Defention() method which we decide
    // not to implement due to the ctr name collision.
    //static APrime * APrime();
};  // class APrime

}  // namespace p348g4

# endif  // GEANT4_MC_MODEL

# endif  // H_APRIME_PARTICLE_DEFINITION_H


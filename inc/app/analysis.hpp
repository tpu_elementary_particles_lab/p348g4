/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348G4_ANALYSIS_APPLICATION_BASE_H
# define H_P348G4_ANALYSIS_APPLICATION_BASE_H

# include "p348g4_config.h"

# ifdef RPC_PROTOCOLS

# include "app/abstract.hpp"
# include "app/mixins/protobuf.hpp"

# include <unordered_map>
# include <unordered_set>

namespace p348 {

# ifdef RPC_PROTOCOLS
/**@class AnalysisApplication
 * @brief Base class for generic-purpose data analysis app of P348 experiment.
 *
 * This mixin implements base for event-by-event processing applications.
 * The pipeline approach implies a series of handlers (here called «processors»)
 * that implement subsequent treatment of data provided in particular event.
 *
 * The common idea of this kind of applications implies sequential reading
 * data from files (event-by-event). Variable parts of them:
 *      - Initial application configuration abstraction; implemented here with
 *      boost::variables_map.
 *      - The data source; we have here at least three types of data formats:
 *          * a ROOT tree
 *          * a "raw data" that has to be decoded by DaqDataDecoding
 *            meanings.
 *          * a compressed universal event format implemented via GPB.
 *      - The postprocessing routine that gathers walkthrough information from
 *      events array.
 *      - The postprocessing routine (including possible output to data streams
 *      or files).
 *
 * This class implements some common basics for configuration and such a sequential
 * reading in order to consuming descendants elaborate its routines.
 *
 * @ingroup app
 * @ingroup analysis
 */
class AnalysisApplication :
        public virtual p348::AbstractApplication,
        public mixins::PBEventApp {
public:
    typedef AbstractApplication Parent;
    typedef typename mixins::PBEventApp::UniEvent Event;
    /// Concrete config object type shortcut (variables map from boost lib).
    typedef Parent::Config Config;
    /// Particular callback type.
    //typedef void*(*Reader)(void*);  // XXX

    /// Data format reader object representation.
    class iEventSequence {
        virtual bool _V_is_good() = 0;
        virtual void _V_next_event( Event *& ) = 0;
        virtual Event * _V_initialize_reading() = 0;
        virtual void _V_finalize_reading() = 0;
        virtual void _V_print_brief_summary( std::ostream & ) const {}
    public:
        virtual ~iEventSequence(){}
        virtual bool is_good() {     return _V_is_good(); }
        virtual Event * initialize_reading() { return _V_initialize_reading(); }
        virtual void next_event( Event *& e ) {         _V_next_event( e ); }
        virtual void finalize_reading( ) {   _V_finalize_reading(); }
        virtual void print_brief_summary( std::ostream & os ) const { _V_print_brief_summary( os ); }
    };

    /// Event processor reader object representation.
    class iEventProcessor {
    protected:
        /// Should return 'false' if processing in chain should be aborted.
        virtual bool _V_process_event( Event * ) = 0;
        /// Called after single event processed by all the processors.
        virtual void _V_finalize_event_processing( Event * ) {}
        /// Called after all events read and source closed to cleanup statistics.
        virtual void _V_finalize() const {}
        /// Called after all events read and all processors finalized.
        virtual void _V_print_brief_summary( std::ostream & ) const {}
    public:
        virtual ~iEventProcessor(){}
        virtual bool operator()( Event * e ) { return _V_process_event( e ); }
        virtual void finalize_event( Event * e ) { _V_finalize_event_processing( e ); }
        virtual void print_brief_summary( std::ostream & os ) const { _V_print_brief_summary( os ); }
        virtual void finalize() const { _V_finalize(); }
    };

    /// Processor for experimental events (shortcut).
    class iExperimantalEventProcessor : public iEventProcessor {
    protected:
        /// Should return 'false' if processing in chain should be aborted.
        virtual bool _V_process_event( Event * ) override;
        virtual bool _V_process_experimental_event( p348::events::ExperimentalEvent * ) = 0;
    };

    /// Processor for SADC profiles in experimental events (shortcut).
    class iSADCProcessor : public iExperimantalEventProcessor {
    protected:
        virtual bool _V_process_experimental_event( p348::events::ExperimentalEvent * ) override;
        virtual bool _V_finalize_processing_sadc_event( p348::events::ExperimentalEvent * ) { return true; }
        virtual bool _V_process_SADC_profile_event( p348::events::SADC_profile * ) = 0;
    };

    /// Processor for APV profiles in experimental events (shortcut).
    class iAPVProcessor : public iExperimantalEventProcessor {
    protected:
        virtual bool _V_process_experimental_event( p348::events::ExperimentalEvent * ) override;
        virtual bool _V_finalize_processing_apv_event( p348::events::ExperimentalEvent * ) { return true; }
        virtual bool _V_process_APV_samples( p348::events::APV_sampleSet * ) = 0;
    };

    typedef iEventSequence *(*EvSeqCtr)();
    typedef iEventProcessor *(*EvProcCtr)();
    typedef po::options_description (*OptionsSupplement)();

    typedef std::list<iEventProcessor *> ProcessorsChain;
private:
    static std::unordered_map<std::string, std::pair<EvSeqCtr, std::string> >  * _readersDict;
    static std::unordered_map<std::string, std::pair<EvProcCtr, std::string> > * _procsDict;
    static std::unordered_set<OptionsSupplement> * _suppOpts;
protected:
    iEventSequence * _evSeq;
    ProcessorsChain _processorsChain;
protected:
    // INTERFACE
    /// Produces concrete application-specific options.
    virtual std::vector<po::options_description> _V_get_options() const override;
    /// Called after common configuration options is done. Can set _immediateExit flag.
    virtual void _V_configure_concrete_app() override;

    /// Shortcut to be invoked by reader callbacks.
    static void treat();
public:
    AnalysisApplication( po::variables_map * vm );
    virtual ~AnalysisApplication();

    /// Inserts reader callback.
    static void add_reader( const std::string &, EvSeqCtr, const std::string & descr );
    /// Prints out available reader callbacks keys.
    static void list_readers( std::ostream & );
    /// Looks for reader callback pointed out by string. Raises notFound on failure.
    static EvSeqCtr find_reader( const std::string & );

    /// Inserts processor callback.
    static void add_processor( const std::string &, EvProcCtr, const std::string & descr );
    /// Prints out available processors keys.
    static void list_processors( std::ostream & );
    /// Looks for processor pointed out by string. Raises notFound on failure.
    static EvProcCtr find_processor( const std::string & );
    /// Adds processor to processor chain.
    void push_back_processor( iEventProcessor * );
    /// Adds processor to processor chain (shortcut for finding by name).
    void push_back_processor( const std::string & );
    /// Returns a processor chain list.
    ProcessorsChain & get_processors_chain() { return _processorsChain; }

    /// Adds options supplement routine.
    static void supp_options( OptionsSupplement );

    /// Return current event sequence ptr.
    template<typename TagetTypeT>
    TagetTypeT get_evseq() {
            return safe_cast<TagetTypeT>( *_evSeq );
        }
};  // class AnalysisApplication

/**@def p348_DEFINE_DATA_SOURCE_FMT_CONSTRUCTOR
 * @brief A data format reader constructor implementation macro. */
# define p348_DEFINE_DATA_SOURCE_FMT_CONSTRUCTOR( ReaderClass )                             \
static void __static_register_ ## ReaderClass ## _fmt() __attribute__ ((constructor(156))); \
static AnalysisApplication::iEventSequence * _static_construct_ ## ReaderClass ()

/**@def p348_REGISTER_DATA_SOURCE_FMT_CONSTRUCTOR
 * @brief A data format reader constructor insertion macro. */
# define p348_REGISTER_DATA_SOURCE_FMT_CONSTRUCTOR( ReaderClass, txtName, descr )           \
static void __static_register_ ## ReaderClass ## _fmt() {                                   \
    AnalysisApplication::add_reader( txtName, _static_construct_ ## ReaderClass, descr ); }

/**@def p348_DEFINE_DATA_PROCESSOR
 * @brief A data processing constructor implementation macro. */
# define p348_DEFINE_DATA_PROCESSOR( ProcessorClass )                                           \
static void __static_register_ ## ProcessorClass ## _prc() __attribute__ ((constructor(156)));  \
static AnalysisApplication::iEventProcessor * _static_construct_ ## ProcessorClass ()

/**@def p348_DEFINE_CONFIG_ARGUMENTS
 * @brief Supplementary configuration insertion macro for \ref AnalysisApplication . */
# define p348_DEFINE_CONFIG_ARGUMENTS                                                       \
static po::options_description _get_supp_options();                                         \
static void __static_register_args() __attribute__ ((constructor(156)));                    \
static void __static_register_args() {                                                      \
    AnalysisApplication::supp_options( _get_supp_options );}                                \
static po::options_description _get_supp_options()

/**@def p348_REGISTER_DATA_SOURCE_FMT_CONSTRUCTOR
 * @brief A data processor constructor insertion macro. */
# define p348_REGISTER_DATA_PROCESSOR( ProcessorClass, txtName, descr )                 \
static void __static_register_ ## ProcessorClass ## _prc() {                            \
    AnalysisApplication::add_processor( txtName, _static_construct_ ## ProcessorClass, descr ); }

# endif  // RPC_PROTOCOLS

}  // namespace p348

# endif  // RPC_PROTOCOLS
# endif  // H_P348G4_ANALYSIS_APPLICATION_BASE_H

